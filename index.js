/**
 * @format
 */
import React from 'react';
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import {name as appName} from './app.json';
import accountTypeReducer from './src/components/AccountTypeSignup/store/reducer/accountTypeReducer';
import signupReducer from './src/containers/Signup/store/reducer/signupReducer';
import forgotPasswordReducer from './src/components/ForgotPassword/store/reducer/forgotPasswordReducer';
import verifyOtpReducer from './src/components/VerifyOtp/store/reducer/verifyOtpReducer';
import resetPasswordReducer from './src/components/ResetPassword/store/reducer/resetPasswordReducer';
import signupInfoReducer from './src/components/SignupInfo/store/reducer/signupInfoReducer';
import signInReducer from './src/containers/SignIn/store/reducer/signInReducer';
import pickPathReducer from './src/components/PickPath/store/reducer/pickPathReducer';
import addPathReducer from './src/components/ExpertHomeTab/AddPath/store/reducer/addPathReducer';
import thunk from 'redux-thunk';

const logger = store => {
  return next => {
    return action => {
      console.log('Action Dispatched', action);
      const result = next(action);
      console.log('Updated State', store.getState());
      return result;
    };
  };
};
const rootReducer = combineReducers({
  signin: signInReducer,
  accountType: accountTypeReducer,
  signup: signupReducer,
  signupInfo: signupInfoReducer,
  forgotPassword: forgotPasswordReducer,
  verifyOtp: verifyOtpReducer,
  resetPassword: resetPasswordReducer,
  pickPath: pickPathReducer,
  addPath: addPathReducer,
});
const store = createStore(rootReducer, applyMiddleware(thunk, logger));
const RNRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
);
AppRegistry.registerComponent(appName, () => RNRedux);
