import React, {Fragment, useEffect} from 'react';
import {Platform, StatusBar, BlurView} from 'react-native';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen';
import IntroSlider from './src/components/IntroSlider/IntroSlider';
import SignIn from './src/containers/SignIn/SignIn';
import Signup from './src/containers/Signup/Signup';
import AccountType from './src/components/AccountTypeSignup/AccountTypeSignup';
import VerifyOtp from './src/components/VerifyOtp/VerifyOtp';
import SignupInfo from './src/components/SignupInfo/SignupInfo';
import ForgotPassword from './src/components/ForgotPassword/ForgotPassword';
import VerifyForgotOtp from './src/components/VerifyForgotOtp/VerifyForgotOtp';
import ResetPassword from './src/components/ResetPassword/ResetPassword';
import AspirantDashboard from './src/containers/AspirantDashboard/AspirantDashboard';
import ExpertDashboard from './src/containers/ExpertDashboard/ExpertDashboard';
import PickPath from './src/components/PickPath/PickPath';
import IntroVideo from './src/components/ExpertHomeTab/AddPath/IntroVideo/IntroVideo';
const Stack = createStackNavigator();
const forFade = ({current}) => ({
  cardStyle: {
    opacity: current.progress,
  },
});
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,

    background: '#fff',
  },
};
const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Fragment>
      {Platform.OS === 'android' && <StatusBar backgroundColor="#fff" />}
      <NavigationContainer theme={MyTheme}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName="Intro">
          <Stack.Screen name="Intro" component={IntroSlider} />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="SignIn"
            component={SignIn}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="AccountType"
            component={AccountType}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="Signup"
            component={Signup}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="VerifyOtp"
            component={VerifyOtp}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="SignupInfo"
            component={SignupInfo}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="ForgotPassword"
            component={ForgotPassword}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="VerifyForgotOtp"
            component={VerifyForgotOtp}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="ResetPassword"
            component={ResetPassword}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="AspirantDashboard"
            component={AspirantDashboard}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="ExpertDashboard"
            component={ExpertDashboard}
          />
          <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="PickPath"
            component={PickPath}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Fragment>
  );
};
export default App;
