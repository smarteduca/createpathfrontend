import React, {PureComponent} from 'react';
import {SafeAreaView, Text, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ActionButton from 'react-native-action-button';
class HomeScreen extends PureComponent {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.subContainer}>
          <ActionButton fixNativeFeedbackRadius={true} buttonColor="#009fee">
            <ActionButton.Item
              textStyle={styles.textStyle}
              buttonColor="#009fee"
              title="Add Path"
              onPress={() => this.props.navigation.navigate('AddPath')}>
              <Icon name="graduation-cap" size={25} color="#fff" />
            </ActionButton.Item>
            <ActionButton.Item
              textStyle={styles.textStyle}
              buttonColor="#009fee"
              title="Add Topic"
              onPress={() => this.props.navigation.navigate('AddTopic')}>
              <Icon name="book" size={25} color="#fff" />
            </ActionButton.Item>
          </ActionButton>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    padding: '6%',
    display: 'flex',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontFamily: 'Roboto',
    color: '#009fee',
    fontSize: 10,
  },
});
export default HomeScreen;
