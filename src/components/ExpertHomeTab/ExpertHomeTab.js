import React, {PureComponent} from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './HomeScreen/HomeScreen';
import AddPath from './AddPath/AddPath';
import AddTopic from './AddTopic/AddTopic';
const HomeStack = createStackNavigator();

class ExpertHomeTab extends PureComponent {
  render() {
    return (
      <HomeStack.Navigator
        screenOptions={{
          headerShown: false,
          animationEnabled: false,
        }}>
        <HomeStack.Screen name="Home" component={HomeScreen} />
        <HomeStack.Screen name="AddPath" component={AddPath} />
        <HomeStack.Screen name="AddTopic" component={AddTopic} />
      </HomeStack.Navigator>
    );
  }
}

export default ExpertHomeTab;
