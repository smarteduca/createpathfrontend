import React, {PureComponent} from 'react';
import {
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {setVideoData, setIntroVideoUrl} from '../store/actions/index';
class IntroVideo extends PureComponent {
  async UNSAFE_componentWillMount() {
    console.log('Intro Video Component Will Mount');
    const pathId = await AsyncStorage.getItem('pathId');
    const token = await AsyncStorage.getItem('token');
    fetch('http://192.168.43.55:8080/' + `expertPath/loadVideoData/${pathId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        console.log('result found', result);
        this.props.setVideoData(result.videoUrl);
        this.setState({preview: true});
      })
      .catch(err => {
        console.log(err);
      });
  }
  state = {
    recording: false,
    preview: false,

    playPreviewVideo: true,
  };

  startRecording = async camera => {
    console.log('Start Recording');
    const {uri} = await camera.recordAsync({
      maxDuration: 120,
    });
    console.log('uri', uri);
    this.props.setVideoData(uri);
    // this.setState({uri});
  };
  stopRecording = camera => {
    console.log('stor recording');
    camera.stopRecording();
  };
  recordingStarted = () => {
    console.log('recording started');
    this.setState({recording: true});
  };
  recordingEnded = () => {
    console.log('recording ended');
    this.setState({preview: true, recording: false});
  };
  recordedVideoEnd = () => {
    console.log('end reached....');
    this.player.seek(0);
    this.setState({playPreviewVideo: false});
  };
  playRecordedVideo = () => {
    console.log('play recorded video');
    this.setState({playPreviewVideo: true});
  };
  stopRecordedVideo = () => {
    console.log('stop recorded video');
    this.setState({playPreviewVideo: false});
  };
  returnActionButton = camera => {
    let actionButtonView;
    if (this.state.recording) {
      actionButtonView = (
        <TouchableOpacity
          style={styles.actionButton}
          onPress={() => this.stopRecording(camera)}>
          <Icon name="microphone" size={25} color="#fff" />
        </TouchableOpacity>
      );
    } else {
      actionButtonView = (
        <TouchableOpacity
          style={styles.actionButton}
          onPress={() => this.startRecording(camera)}>
          <Icon name="video" size={25} color="#fff" />
        </TouchableOpacity>
      );
    }
    return actionButtonView;
  };
  retakeVideo = () => {
    this.setState({recording: false, preview: false});
    this.props.setVideoData(null);
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          {!this.state.preview ? (
            <RNCamera
              onRecordingStart={this.recordingStarted}
              onRecordingEnd={this.recordingEnded}
              style={styles.preview}
              autoFocus={RNCamera.Constants.AutoFocus.on}
              type={RNCamera.Constants.Type.front}
              flashMode={RNCamera.Constants.FlashMode.on}>
              {({camera, status, recordAudioPermissionStatus}) => {
                return (
                  <View style={styles.actionsView}>
                    {this.returnActionButton(camera)}
                  </View>
                );
              }}
            </RNCamera>
          ) : (
            <View style={styles.container}>
              <Video
                ref={ref => (this.player = ref)}
                paused={!this.state.playPreviewVideo}
                onEnd={() => this.recordedVideoEnd()}
                source={{uri: this.props.videoData}}
                style={styles.previewVideo}
                resizeMode="stretch"
              />
              <View style={styles.previewActionView}>
                <TouchableOpacity
                  style={styles.actionButton}
                  onPress={() => {
                    this.state.playPreviewVideo
                      ? this.stopRecordedVideo()
                      : this.playRecordedVideo();
                  }}>
                  <Icon
                    name={this.state.playPreviewVideo ? 'pause' : 'play'}
                    size={25}
                    color="#fff"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.actionButton}
                  onPress={() => this.retakeVideo()}>
                  <Icon name="redo" size={25} color="#fff" />
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    height: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  actionsView: {
    height: '15%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionButton: {
    backgroundColor: 'rgb(37,111,238)',
    width: '15%',
    height: '80%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },
  previewVideo: {
    height: '90%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  previewActionView: {
    display: 'flex',
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});
const mapStateToProps = state => {
  return {
    videoData: state.addPath.videoData,
    introVideoUrl: state.addPath.introVideoUrl,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setVideoData: data => dispatch(setVideoData(data)),
    setIntroVideoUrl: data => dispatch(setIntroVideoUrl(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IntroVideo);
