import React, {PureComponent} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
  View,
  Modal,
  Dimensions,
  FlatList,
} from 'react-native';
import KeyboardShift from '../../../KeyboardShift/KeyboardShift';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import {
  loadBasicInfo,
  setPathTitle,
  setPathTag,
  setPathDescription,
} from '../store/actions/index';
import AsyncStorage from '@react-native-community/async-storage';
import {loadPaths} from '../../../PickPath/store/actions/index';

class BasicInfo extends PureComponent {
  async UNSAFE_componentWillMount() {
    this.props.loadPaths();
    try {
      const pathId = await AsyncStorage.getItem('pathId');
      const token = await AsyncStorage.getItem('token');
      if (pathId) {
        const data = {
          token,
          pathId,
        };
        this.props.loadBasicInfo(data);
      }
    } catch (err) {
      console.log(err);
    }
  }
  state = {
    modalVisible: false,
  };
  openPathsModal = () => {
    this.setState({modalVisible: true});
  };
  closeModal = () => {
    this.setState({modalVisible: false});
  };
  selectTag = tag => {
    this.props.setPathTag(tag);
    this.setState({modalVisible: false});
  };
  render() {
    return (
      <KeyboardShift>
        {() => (
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <SafeAreaView style={styles.container}>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}>
                <SafeAreaView style={styles.modalContainer}>
                  <View style={styles.modalSubContainer}>
                    <Icon
                      name="times"
                      size={30}
                      style={styles.modalCloseIcon}
                      onPress={() => this.closeModal()}
                    />
                    <FlatList
                      getItemLayout={(data, index) => ({
                        length: 60,
                        offset: 60 * index,
                        index,
                      })}
                      style={styles.modalList}
                      data={this.props.paths}
                      renderItem={({item}) => (
                        <TouchableWithoutFeedback
                          onPress={() => this.selectTag(item)}>
                          <View style={styles.listView}>
                            <Text style={styles.listText}> {item.name} </Text>
                          </View>
                        </TouchableWithoutFeedback>
                      )}
                      keyExtractor={item => item.name}
                    />
                  </View>
                </SafeAreaView>
              </Modal>
              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}
                style={styles.subContainer}>
                <TextInput
                  value={this.props.title}
                  onChangeText={text => this.props.setPathTitle(text)}
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Path Title"
                />
                <TouchableWithoutFeedback onPress={() => this.openPathsModal()}>
                  <View style={styles.tagInput}>
                    {this.props.tag != null ? (
                      <Text style={styles.selectedText}>
                        {' '}
                        {this.props.tag.name}{' '}
                      </Text>
                    ) : (
                      <Text style={styles.tagText}> Select Path Tag </Text>
                    )}
                    <Icon
                      name="tag"
                      color="rgb(227,227,227)"
                      style={styles.tagIcon}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TextInput
                  multiline={true}
                  numberOfLines={4}
                  value={this.props.description}
                  onChangeText={text => this.props.setPathDescription(text)}
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Enter Short Description"
                />
              </ScrollView>
            </SafeAreaView>
          </TouchableWithoutFeedback>
        )}
      </KeyboardShift>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',
    height: '100%',
    width: '100%',
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  tagInput: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginTop: 60,
    marginBottom: 30,
    paddingBottom: 20,
  },
  tagText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(227,227,227)',
  },
  selectedText: {
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  tagIcon: {
    fontSize: 20,
    alignSelf: 'center',
  },
  modalContainer: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: '#fff',
  },
  modalSubContainer: {
    padding: '6%',
    display: 'flex',
    height: '100%',
    width: '100%',
  },
  modalCloseIcon: {
    alignSelf: 'flex-end',
  },
  modalList: {
    flexGrow: 0,
    width: '100%',
    height: '75%',
    marginTop: '25%',
  },
  listText: {
    fontFamily: 'Roboto',
    fontSize: 20,
    color: 'rgb(120,120,120)',
    fontWeight: '700',
  },
  listView: {
    display: 'flex',
    justifyContent: 'center',

    height: 60,
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
  },
});
const mapStateToProps = state => {
  return {
    title: state.addPath.title,
    description: state.addPath.shortDesc,
    tag: state.addPath.tag,
    paths: state.pickPath.paths,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setPathTitle: data => dispatch(setPathTitle(data)),
    setPathDescription: data => dispatch(setPathDescription(data)),
    setPathTag: data => dispatch(setPathTag(data)),
    loadPaths: () => dispatch(loadPaths()),
    loadBasicInfo: data => dispatch(loadBasicInfo(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BasicInfo);
