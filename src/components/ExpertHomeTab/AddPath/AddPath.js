import React, {PureComponent} from 'react';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  ToastAndroid,
  Alert,
} from 'react-native';
import BasicInfo from './BasicInfo/BasicInfo';
import Prerequisite from './Prerequisite/Prerequisite';
import AdditionalInfo from './AdditionalInfo/AdditionalInfo';
import IntroVideo from './IntroVideo/IntroVideo';
import Loader from '../../UI/Loader/Loader';
import validator from 'validator';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {
  resetError,
  loadBasicInfo,
  createExpertPath,
  updateBasicInfo,
  resetBasicInfo,
  resetPrerequisite,
  loadPrerequisites,
  updatePrerequisites,
  resetAdditionalInfo,
  updateAdditionalInfo,
} from './store/actions/index';
class AddPath extends PureComponent {
  state = {
    currentStep: 0,
    progess: 0,
  };
  getHeading = () => {
    const currentStep = this.state.currentStep;
    let headingText;
    switch (currentStep) {
      case 0:
        headingText = <Text style={styles.headingText}> Basic Info </Text>;
        break;
      case 1:
        headingText = <Text style={styles.headingText}> Prerequisite </Text>;
        break;
      case 2:
        headingText = <Text style={styles.headingText}> AdditionalInfo </Text>;
        break;
      case 3:
        headingText = <Text style={styles.headingText}> IntroVideo </Text>;
        break;
    }
    return headingText;
  };
  prevView = () => {
    this.setState(prevState => {
      return {
        ...prevState,
        currentStep: prevState.currentStep - 1,
      };
    });
  };
  nextView = async () => {
    const currentStep = this.state.currentStep;
    const pathId = await AsyncStorage.getItem('pathId');
    const token = await AsyncStorage.getItem('token');
    if (currentStep === 0 && pathId === null) {
      const data = {
        title: this.props.title,
        token,
        tagId: this.props.tag._id,
        shortDescription: this.props.shortDesc,
      };
      this.props.createExpertPath(data);
    }
    if (currentStep === 0 && pathId != null) {
      const data = {
        token,
        expertPathId: pathId,
        updatedTagId: this.props.tag._id,
        updatedTitle: this.props.title,
        updatedShortDescription: this.props.shortDesc,
      };
      this.props.updateBasicInfo(data);
    }
    if (currentStep === 1 && pathId != null) {
      const data = {
        token,
        expertPathId: pathId,
        prerequisites: this.props.prerequisites,
      };
      this.props.updatePrerequisites(data);
    }
    if (currentStep === 2 && pathId != null) {
      const data = {
        token,
        expertPathId: pathId,
        planType: this.props.paymentType,
        releventExperience: this.props.releventExperience,
      };
      this.props.updateAdditionalInfo(data);
    }
    if (currentStep === 3 && pathId != null) {
      // this.setState(prevState => {
      //   return {
      //     ...prevState,
      //     currentStep: prevState.currentStep + 1,
      //   };
      // });
      const data = new FormData();
      data.append('introVideo', {
        name: 'introVideo',
        type: 'video/mp4',
        uri: this.props.videoData,
      });
      fetch(
        'http://192.168.43.55:8080/' + `expertPath/uploadIntroVideo/${pathId}`,
        {
          method: 'POST',
          body: data,
          headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      )
        .then(res => res.json())
        .then(response => {
          console.log(response);
        })
        .catch(err => {
          console.log(err);
        });
    }
  };
  getMainView = () => {
    const currentStep = this.state.currentStep;
    let mainView;
    switch (currentStep) {
      case 0:
        mainView = <BasicInfo />;
        break;
      case 1:
        mainView = <Prerequisite />;
        break;
      case 2:
        mainView = <AdditionalInfo />;
        break;
      case 3:
        mainView = <IntroVideo />;
        break;
    }
    return mainView;
  };
  getNextDisabledStatus = () => {
    let isDisabled = false;
    const currentStep = this.state.currentStep;
    switch (currentStep) {
      case 0:
        if (
          validator.isEmpty(this.props.shortDesc, {
            ignore_whitespace: true,
          }) ||
          validator.isEmpty(this.props.title, {
            ignore_whitespace: true,
          }) ||
          this.props.tag === null
        ) {
          isDisabled = true;
        }
        break;
      case 1:
        this.props.prerequisites.forEach(prerequisite => {
          if (validator.isEmpty(prerequisite.name, {ignore_whitespace: true})) {
            isDisabled = true;
          }
        });
        break;
      case 2:
        if (
          validator.isEmpty(this.props.releventExperience, {
            ignore_whitespace: true,
          })
        ) {
          isDisabled = true;
        }
        break;
      case 3:
        if (this.props.videoData === null) {
          isDisabled = true;
        }
        break;
    }
    return isDisabled;
  };
  getNextButtonStyles = () => {
    let nextButtonStyles = [styles.nextButton];
    const currentStep = this.state.currentStep;
    switch (currentStep) {
      case 0:
        if (
          validator.isEmpty(this.props.shortDesc, {
            ignore_whitespace: true,
          }) ||
          validator.isEmpty(this.props.title, {
            ignore_whitespace: true,
          }) ||
          this.props.tag === null
        ) {
          nextButtonStyles = [styles.disableCss, styles.nextButton];
        }
        break;
      case 1:
        this.props.prerequisites.forEach(prerequisite => {
          if (validator.isEmpty(prerequisite.name, {ignore_whitespace: true})) {
            nextButtonStyles = [styles.disableCss, styles.nextButton];
          }
        });
        break;
      case 2:
        if (
          validator.isEmpty(this.props.releventExperience, {
            ignore_whitespace: true,
          })
        ) {
          nextButtonStyles = [styles.disableCss, styles.nextButton];
        }
        break;
      case 3:
        if (this.props.videoData === null) {
          nextButtonStyles = [styles.disableCss, styles.nextButton];
        }
        break;
    }
    return nextButtonStyles;
  };
  render() {
    const currentStep = this.state.currentStep;
    let backButtonStyles = [styles.backButton];
    let backButtonDisabled = false;
    if (currentStep === 0) {
      backButtonDisabled = true;
      backButtonStyles = [styles.backButton, styles.disableCss];
    }
    if (this.props.basicInfoSuccess) {
      this.setState(prevState => {
        return {
          ...prevState,
          currentStep: prevState.currentStep + 1,
        };
      });
      this.props.resetBasicInfo();
    }
    if (this.props.prerequisiteSuccess) {
      this.setState(prevState => {
        return {
          ...prevState,
          currentStep: prevState.currentStep + 1,
        };
      });
      this.props.resetPrerequisite();
    }
    if (this.props.additionalInfoSuccess) {
      this.setState(prevState => {
        return {
          ...prevState,
          currentStep: prevState.currentStep + 1,
        };
      });
      this.props.resetAdditionalInfo();
    }
    if (this.props.error && Platform.OS === 'android') {
      ToastAndroid.show(this.props.error, ToastAndroid.SHORT);
      this.props.resetError();
    }
    if (this.props.error && Platform.OS === 'ios') {
      Alert.alert(this.props.error);
      this.props.resetError();
    }
    return (
      <SafeAreaView style={styles.container}>
        {this.props.ongoingRequest && <Loader />}
        <View style={styles.subContainer}>
          <View style={styles.headerSection}>
            <View style={{flexBasis: '20%'}}>
              <AnimatedCircularProgress
                rotation={0}
                size={70}
                width={10}
                fill={this.state.progess}
                tintColor="#009fee"
                onAnimationComplete={() => console.log('onAnimationComplete')}
                backgroundColor="rgb(227,227,227)"
              />
            </View>
            <View style={styles.headingTextContainer}>{this.getHeading()}</View>
          </View>
          <View style={styles.mainViewSection}>{this.getMainView()}</View>
          <View style={styles.footerSection}>
            <TouchableOpacity
              onPress={() => this.prevView()}
              style={backButtonStyles}
              disabled={backButtonDisabled}>
              <Text style={styles.backButtonText}> Back </Text>
            </TouchableOpacity>
            <TouchableOpacity
              disabled={this.getNextDisabledStatus()}
              style={this.getNextButtonStyles()}
              // style={styles.nextButton}
              onPress={() => this.nextView()}>
              <Text style={styles.nextButtonText}> Next </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  disableCss: {
    opacity: 0.4,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
    padding: '4%',
  },
  headerSection: {
    display: 'flex',
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingTextContainer: {
    display: 'flex',
    width: '80%',
    height: '100%',
    paddingLeft: '6%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  headingText: {
    color: 'rgb(51,51,51)',
    fontSize: 25,
    fontFamily: 'Roboto',
    fontWeight: '500',
    textTransform: 'capitalize',
  },
  mainViewSection: {
    height: '70%',
  },
  footerSection: {
    display: 'flex',
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  backButton: {
    backgroundColor: 'rgb(224,234,252)',
    width: '30%',
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  backButtonText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(37,111,238)',
    fontWeight: 'bold',
  },
  nextButton: {
    backgroundColor: 'rgb(37,111,238)',
    width: '30%',
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: '#fff',
    fontWeight: 'bold',
  },
});
const mapStateToProps = state => {
  return {
    shortDesc: state.addPath.shortDesc,
    tag: state.addPath.tag,
    title: state.addPath.title,
    error: state.addPath.error,
    basicInfoSuccess: state.addPath.basicInfoSuccess,
    ongoingRequest: state.addPath.ongoingRequest,
    prerequisiteSuccess: state.addPath.prerequisiteSuccess,
    prerequisites: state.addPath.prerequisites,
    additionalInfoSuccess: state.addPath.additionalInfoSuccess,
    releventExperience: state.addPath.releventExperience,
    paymentType: state.addPath.paymentType,
    videoData: state.addPath.videoData,
    introVideoUrl: state.addPath.introVideoUrl,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetError: () => dispatch(resetError()),
    loadBasicInfo: data => dispatch(loadBasicInfo(data)),
    createExpertPath: data => dispatch(createExpertPath(data)),
    updateBasicInfo: data => dispatch(updateBasicInfo(data)),
    resetBasicInfo: () => dispatch(resetBasicInfo()),
    resetPrerequisite: () => dispatch(resetPrerequisite()),
    loadPrerequisites: data => dispatch(loadPrerequisites(data)),
    updatePrerequisites: data => dispatch(updatePrerequisites(data)),
    resetAdditionalInfo: () => dispatch(resetAdditionalInfo()),
    updateAdditionalInfo: data => dispatch(updateAdditionalInfo(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddPath);
