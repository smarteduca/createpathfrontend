import React, {PureComponent} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import {
  setPaymentType,
  setReleventExperience,
  loadAdditionalInfo,
} from '../store/actions/index';
import AsyncStorage from '@react-native-community/async-storage';
class AdditionalInfo extends PureComponent {
  async UNSAFE_componentWillMount() {
    try {
      const pathId = await AsyncStorage.getItem('pathId');
      const token = await AsyncStorage.getItem('token');
      if (pathId) {
        const data = {
          token,
          pathId,
        };
        this.props.loadAdditionalInfo(data);
      }
    } catch (err) {
      console.log(err);
    }
  }
  state = {
    paymentType: ['free', 'paid'],
  };
  displayPaymentTypeOptions = () => {
    return this.state.paymentType.map(type => {
      if (type == this.props.paymentType) {
        return (
          <TouchableOpacity
            onPress={() => this.props.setPaymentType(type)}
            key={type}
            style={styles.activePaymentOption}>
            <Text style={styles.activePaymentText}> {type} </Text>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            key={type}
            style={styles.paymentOption}
            onPress={() => this.props.setPaymentType(type)}>
            <Text style={styles.paymentOptionText}> {type} </Text>
          </TouchableOpacity>
        );
      }
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
          <View style={styles.subContainer}>
            <Text style={styles.planTypeHeading}> Choose Plan Type </Text>
            <View style={styles.paymentTypeView}>
              {this.displayPaymentTypeOptions()}
            </View>
            <TextInput
              multiline={true}
              numberOfLines={5}
              value={this.props.releventExperience}
              onChangeText={text => this.props.setReleventExperience(text)}
              placeholderTextColor="rgb(227,227,227)"
              style={styles.input}
              placeholder="Enter Relevent Experience"
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  planTypeHeading: {
    fontFamily: 'Roboto',
    fontSize: 20,
    textTransform: 'capitalize',
    marginTop: '4%',
    fontStyle: 'italic',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  subContainer: {
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  paymentTypeView: {
    marginTop: '10%',
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 70,
  },
  paymentOption: {
    backgroundColor: 'rgb(224,234,252)',
    width: '30%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  paymentOptionText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(37,111,238)',
    textTransform: 'capitalize',
  },
  activePaymentOption: {
    backgroundColor: 'rgb(37,111,238)',
    width: '30%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  activePaymentText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: '#fff',
    textTransform: 'capitalize',
  },
});
const mapStateToProps = state => {
  return {
    paymentType: state.addPath.paymentType,
    releventExperience: state.addPath.releventExperience,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setPaymentType: data => dispatch(setPaymentType(data)),
    setReleventExperience: data => dispatch(setReleventExperience(data)),
    loadAdditionalInfo: data => dispatch(loadAdditionalInfo(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AdditionalInfo);
