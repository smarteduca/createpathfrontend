import * as actions from './actionTypes';
import env from '../../../../../config/environment';
import AsyncStorage from '@react-native-community/async-storage';
export const setPathTitle = value => {
  return {
    type: actions.ADD_PATH_SET_TITLE,
    value,
  };
};
export const setPathTag = value => {
  return {
    type: actions.ADD_PATH_SET_PATH_TAG,
    value,
  };
};
export const setPathDescription = value => {
  return {
    type: actions.ADD_PATH_SET_SHORT_DESC,
    value,
  };
};
export const setIntroVideoUrl = value => {
  return {
    type: actions.ADD_PATH_SET_INTRO_VIDEO_URL,
    value,
  };
};
const setOngoingRequest = value => {
  return {
    type: actions.ADD_PATH_SET_ONGOING_REQUEST,
    value,
  };
};

export const setError = error => {
  return {
    type: actions.ADD_PATH_ERROR,
    error: error,
  };
};
export const resetError = () => {
  return {
    type: actions.ADD_PATH_RESET_ERROR,
  };
};
const addPathBasicInfoSuccess = () => {
  return {
    type: actions.ADD_PATH_BASIC_INFO_SUCCESS,
  };
};
const addPathPrerequisiteSuccess = () => {
  return {
    type: actions.ADD_PATH_PREREQUISITE_SUCCESS,
  };
};
const addpathAdditionalSuccess = () => {
  return {
    type: actions.ADD_PATH_ADDITIONAL_INFO_SUCCESS,
  };
};
export const createExpertPath = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'expertPath/add', {
      method: 'POST',
      body: JSON.stringify({
        tagId: data.tagId,
        title: data.title,
        shortDescription: data.shortDescription,
      }),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          AsyncStorage.setItem('pathId', result.createdPathId)
            .then(result => {
              console.log(result);
            })
            .catch(err => {
              console.log(err);
            });
          dispatch(addPathBasicInfoSuccess());
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};
export const updateAdditionalInfo = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'expertPath/updateAdditionalInfo', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(addpathAdditionalSuccess());
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};
export const updatePrerequisites = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'expertPath/updatePrerequisites', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(addPathPrerequisiteSuccess());
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};
export const updateBasicInfo = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'expertPath/updateBasicInfo', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(addPathBasicInfoSuccess());
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};

export const resetBasicInfo = () => {
  return {
    type: actions.ADD_PATH_RESET_BASIC_INFO,
  };
};
export const resetPrerequisite = () => {
  return {
    type: actions.ADD_PATH_RESET_PREREQUISITE,
  };
};
export const resetAdditionalInfo = () => {
  return {
    type: actions.ADD_PATH_RESET_ADDITIONAL_INFO,
  };
};
const initBasicInfo = data => {
  return {
    type: actions.ADD_PATH_INIT_BASIC_INFO,
    data,
  };
};
const initPrerequisites = value => {
  return {
    type: actions.ADD_PATH_INIT_PREREQUISITES,
    value,
  };
};
const initAdditionalInfo = data => {
  return {
    type: actions.ADD_PATH_INIT_ADDITIONAL_INFO,
    data,
  };
};
export const loadAdditionalInfo = data => {
  return dispatch => {
    fetch(env.API_URL + `expertPath/loadAdditionalInfo/${data.pathId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (
          result.success === true &&
          result.planType != undefined &&
          result.releventExperience != undefined
        ) {
          dispatch(
            initAdditionalInfo({
              planType: result.planType,
              releventExperience: result.releventExperience,
            }),
          );
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};
export const loadPrerequisites = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + `expertPath/loadPrerequisites/${data.pathId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(initPrerequisites(result.prerequisites));
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something Went Wrong'));
      });
  };
};
export const loadBasicInfo = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + `expertPath/loadBasicInfo/${data.pathId}`, {
      method: 'GET',

      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${data.token}`,
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(initBasicInfo(result.details));
        } else {
          dispatch(setError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(setError('Something went wrong'));
      });
  };
};
export const addPrequisiteTopic = () => {
  return {
    type: actions.ADD_PATH_ADD_PREREQUISITE_TOPIC,
  };
};
export const deletePrerequisiteTopic = index => {
  return {
    type: actions.ADD_PATH_DELETE_PREPREQUISITE_TOPIC,
    index,
  };
};
export const updatePrerequisiteTopicName = data => {
  return {
    type: actions.ADD_PATH_UPDATE_PREREQUISITE_TOPIC_NAME,
    data,
  };
};
export const updatePrerequisiteTopicLevel = data => {
  return {
    type: actions.ADD_PATH_UPDATE_PREREQUISITE_TOPIC_LEVEL,
    data,
  };
};
export const setPaymentType = value => {
  return {
    type: actions.ADD_PATH_SET_PAYMENT_TYPE,
    value,
  };
};
export const setReleventExperience = value => {
  return {
    type: actions.ADD_PATH_SET_RELEVENT_EXPERIENCE,
    value,
  };
};
export const setVideoData = value => {
  return {
    type: actions.ADD_PATH_SET_VIDEO_DATA,
    value,
  };
};
