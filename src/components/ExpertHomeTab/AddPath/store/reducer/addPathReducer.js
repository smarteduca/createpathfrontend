import * as actions from '../actions/actionTypes';
const initialState = {
  error: false,
  basicInfoSuccess: false,
  prerequisiteSuccess: false,
  additionalInfoSuccess: false,
  ongoingRequest: false,
  title: '',
  tag: null,
  shortDesc: '',
  prerequisites: [],
  paymentType: 'free',
  releventExperience: '',
  videoData: null,
  introVideoUrl: null,
};
const addPathReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADD_PATH_SET_INTRO_VIDEO_URL:
      return {
        ...state,
        introVideoUrl: action.value,
      };
    case actions.ADD_PATH_SET_VIDEO_DATA:
      return {
        ...state,
        videoData: action.value,
      };
    case actions.ADD_PATH_RESET_ADDITIONAL_INFO:
      return {
        ...state,
        additionalInfoSuccess: false,
        paymentType: 'free',
        releventExperience: '',
      };
    case actions.ADD_PATH_INIT_ADDITIONAL_INFO:
      return {
        ...state,
        releventExperience: action.data.releventExperience,
        paymentType: action.data.planType,
      };
    case actions.ADD_PATH_SET_RELEVENT_EXPERIENCE:
      return {
        ...state,
        releventExperience: action.value,
      };
    case actions.ADD_PATH_SET_PAYMENT_TYPE:
      return {
        ...state,
        paymentType: action.value,
      };
    case actions.ADD_PATH_ADDITIONAL_INFO_SUCCESS:
      return {
        ...state,
        additionalInfoSuccess: true,
      };
    case actions.ADD_PATH_PREREQUISITE_SUCCESS:
      return {
        ...state,
        prerequisiteSuccess: true,
      };
    case actions.ADD_PATH_UPDATE_PREREQUISITE_TOPIC_LEVEL:
      return {
        ...state,
        prerequisites: state.prerequisites.map((prerequisite, index) => {
          if (index != action.data.index) {
            return prerequisite;
          }
          let updatedPrerequisite = {...prerequisite};
          updatedPrerequisite.level = action.data.level;
          return updatedPrerequisite;
        }),
      };
    case actions.ADD_PATH_UPDATE_PREREQUISITE_TOPIC_NAME:
      return {
        ...state,
        prerequisites: state.prerequisites.map((prerequisite, index) => {
          if (index != action.data.index) {
            return prerequisite;
          }
          let updatedPrerequisite = {...prerequisite};
          updatedPrerequisite.name = action.data.text;
          return updatedPrerequisite;
        }),
      };
    case actions.ADD_PATH_DELETE_PREPREQUISITE_TOPIC:
      return {
        ...state,
        prerequisites: state.prerequisites.filter(
          (_, index) => index != action.index,
        ),
      };
    case actions.ADD_PATH_ADD_PREREQUISITE_TOPIC:
      return {
        ...state,
        prerequisites: state.prerequisites.concat({
          name: '',
          level: 'beginner',
        }),
      };
    case actions.ADD_PATH_BASIC_INFO_SUCCESS:
      return {
        ...state,
        basicInfoSuccess: true,
      };
    case actions.ADD_PATH_INIT_PREREQUISITES:
      return {
        ...state,
        prerequisites: action.value,
      };
    case actions.ADD_PATH_INIT_BASIC_INFO:
      return {
        ...state,
        title: action.data.title,
        tag: action.data.tag,
        shortDesc: action.data.shortDesc,
      };
    case actions.ADD_PATH_RESET_PREREQUISITE:
      return {
        ...state,
        prerequisiteSuccess: false,
        prerequisites: [],
      };
    case actions.ADD_PATH_RESET_BASIC_INFO:
      return {
        ...state,
        basicInfoSuccess: false,
        updateBasicInfoSuccess: false,
        ongoingRequest: false,
        title: '',
        tag: null,
        shortDesc: '',
        basicInfoSuccess: false,
      };

    case actions.ADD_PATH_SET_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.ADD_PATH_SET_SHORT_DESC:
      return {
        ...state,
        shortDesc: action.value,
      };
    case actions.ADD_PATH_SET_PATH_TAG:
      return {
        ...state,
        tag: action.value,
      };
    case actions.ADD_PATH_SET_TITLE:
      return {
        ...state,
        title: action.value,
      };

    default:
      return state;
  }
};
export default addPathReducer;
