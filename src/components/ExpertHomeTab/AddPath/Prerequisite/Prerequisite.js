import React, {PureComponent, Fragment} from 'react';
import {
  SafeAreaView,
  Text,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import KeyboardShift from '../../../KeyboardShift/KeyboardShift';
import {connect} from 'react-redux';
import {
  loadPrerequisites,
  addPrequisiteTopic,
  deletePrerequisiteTopic,
  updatePrerequisiteTopicName,
  updatePrerequisiteTopicLevel,
} from '../store/actions/index';
import AsyncStorage from '@react-native-community/async-storage';
class Prerequisite extends PureComponent {
  async UNSAFE_componentWillMount() {
    try {
      const pathId = await AsyncStorage.getItem('pathId');
      const token = await AsyncStorage.getItem('token');
      if (pathId) {
        const data = {
          token,
          pathId,
        };
        this.props.loadPrerequisites(data);
      }
    } catch (err) {
      console.log(err);
    }
  }
  getPrerequisites = () => {
    return this.props.prerequisites.map((prerequisite, index) => {
      return (
        <Fragment>
          <TextInput
            onChangeText={text =>
              this.props.updatePrerequisiteTopicName({index, text})
            }
            value={prerequisite.name}
            placeholderTextColor="rgb(227,227,227)"
            style={styles.input}
            placeholder="Topic Name"
          />
          <Text style={styles.levelHeading}>Pick A Proficiency Level</Text>
          <View style={styles.levelView}>
            {['beginner', 'average', 'expert'].map(level => {
              let active = false;
              if (prerequisite.level === level) {
                active = true;
              }
              return (
                <TouchableOpacity
                  onPress={() =>
                    this.props.updatePrerequisiteTopicLevel({
                      index,
                      level,
                    })
                  }
                  key={level}
                  style={
                    active ? styles.activelevelOption : styles.levelOption
                  }>
                  <Text
                    style={
                      active ? styles.activelevelText : styles.levelOptionText
                    }>
                    {' '}
                    {level}{' '}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <TouchableOpacity
            onPress={() => this.props.deletePrerequisiteTopic(index)}
            style={styles.deleteButton}>
            <Text style={styles.addButtonText}> Delete </Text>
          </TouchableOpacity>
        </Fragment>
      );
    });
  };
  render() {
    return (
      <KeyboardShift>
        {() => (
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <SafeAreaView style={styles.container}>
              <ScrollView
                nestedScrollEnabled={true}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}
                style={styles.subContainer}>
                <Text style={styles.heading}>
                  {' '}
                  Enter Topics needed to learn this path{' '}
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.addPrequisiteTopic()}
                  style={styles.addButton}>
                  <Text style={styles.addButtonText}> Add Topic </Text>
                </TouchableOpacity>

                {this.getPrerequisites()}
              </ScrollView>
            </SafeAreaView>
          </TouchableWithoutFeedback>
        )}
      </KeyboardShift>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  subContainer: {
    display: 'flex',

    height: '100%',
    width: '100%',
  },
  heading: {
    fontFamily: 'Roboto',
    fontSize: 20,
    textTransform: 'capitalize',
    marginTop: '4%',
    fontStyle: 'italic',
  },
  addButton: {
    marginTop: '6%',
    backgroundColor: 'rgb(37,111,238)',
    width: '30%',
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  addButtonText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: '#fff',
    fontWeight: 'bold',
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  levelHeading: {
    fontFamily: 'Roboto',
    fontWeight: '700',
    fontSize: 15,
  },
  levelView: {
    marginTop: '5%',
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 70,
  },
  levelOption: {
    backgroundColor: 'rgb(224,234,252)',
    width: '30%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  levelOptionText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(37,111,238)',
    textTransform: 'capitalize',
  },
  activelevelOption: {
    backgroundColor: 'rgb(37,111,238)',
    width: '30%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  activelevelText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: '#fff',
    textTransform: 'capitalize',
  },
  deleteButton: {
    marginTop: '6%',
    backgroundColor: 'red',
    width: '30%',
    height: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});
const mapStateToProps = state => {
  return {
    prerequisites: state.addPath.prerequisites,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addPrequisiteTopic: () => dispatch(addPrequisiteTopic()),
    deletePrerequisiteTopic: data => dispatch(deletePrerequisiteTopic(data)),
    updatePrerequisiteTopicName: data =>
      dispatch(updatePrerequisiteTopicName(data)),
    updatePrerequisiteTopicLevel: data =>
      dispatch(updatePrerequisiteTopicLevel(data)),
    loadPrerequisites: data => dispatch(loadPrerequisites(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Prerequisite);
