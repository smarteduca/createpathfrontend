import * as actions from '../actions/actionTypes';
const initialState = {
  ongoingRequest: false,
  resendCodeSuccess: false,
  success: false,
  error: null,
};
const verifyOtpReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.VERIFY_OTP_SET_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.VERIFY_OTP_RESET_RESEND_CODE_SUCCESS:
      return {
        ...state,
        resendCodeSuccess: false,
      };
    case actions.VERIFY_OTP_RESEND_CODE_SUCCESS:
      return {
        ...state,
        resendCodeSuccess: true,
      };
    case actions.VERIFY_OTP_RESET_SUCCESS:
      return {
        ...state,
        success: false,
      };
    case actions.VERIFY_OTP_RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    case actions.VERIFY_OTP_ERROR:
      return {
        ...state,
        error: action.value,
      };
    case actions.VERIFY_OTP_SUCCESS:
      return {
        ...state,
        success: action.value,
      };
    default:
      return state;
  }
};
export default verifyOtpReducer;
