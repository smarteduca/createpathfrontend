import * as actions from './actionTypes';
import env from '../../../../config/environment';
const setOngoingRequest = value => {
  return {
    type: actions.VERIFY_OTP_SET_ONGOING_REQUEST,
    value,
  };
};
const otpVerifySuccess = value => {
  return {
    type: actions.VERIFY_OTP_SUCCESS,
    value,
  };
};
const otpVerifyError = err => {
  return {
    type: actions.VERIFY_OTP_ERROR,
    value: err,
  };
};
export const verifyOtp = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'verify/verifyCode', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOngoingRequest(false));
        console.log(result);
        if (result.verification_status.status === 'approved') {
          console.log('approved');
          dispatch(otpVerifySuccess(true));
        } else {
          console.log('invalid code');
          dispatch(otpVerifyError('Invalid Code'));
        }
      })
      .catch(err => {
        console.log('Invalid JSON');
        dispatch(setOngoingRequest(false));
        dispatch(otpVerifyError('Invalid JSON'));
      });
  };
};
export const resetSuccess = () => {
  return {
    type: actions.VERIFY_OTP_RESET_SUCCESS,
  };
};
export const resetError = () => {
  return {
    type: actions.VERIFY_OTP_RESET_ERROR,
  };
};
const resendCodeSuccess = () => {
  return {
    type: actions.VERIFY_OTP_RESEND_CODE_SUCCESS,
  };
};

export const resendCodeVerifyOtp = data => {
  return dispatch => {
    fetch(env.API_URL + 'verify/sendVerification', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        console.log(result);
        if (result.verification.status === 'pending') {
          dispatch(resendCodeSuccess(true));
        } else {
          dispatch(otpVerifyError('Something went wrong'));
        }
      })
      .catch(err => {
        dispatch(otpVerifyError('Invalid JSON'));
      });
  };
};
export const resetResendCodeSuccess = () => {
  return {
    type: actions.VERIFY_OTP_RESET_RESEND_CODE_SUCCESS,
  };
};
