import * as actions from '../actions/actionTypes';
const initialState = {
  type: '',
};
const accountTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_ACCOUNT_TYPE:
      return {
        ...state,
        type: action.value,
      };
    default:
      return state;
  }
};
export default accountTypeReducer;
