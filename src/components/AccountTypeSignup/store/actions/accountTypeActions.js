import * as actions from './actionTypes';
export const setAccountType = value => {
  return {
    type: actions.SET_ACCOUNT_TYPE,
    value: value,
  };
};
