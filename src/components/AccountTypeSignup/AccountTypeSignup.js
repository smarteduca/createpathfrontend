import React from 'react';
import {
  TouchableWithoutFeedback,
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {setAccountType} from './store/actions';
const AccountTypeSignup = props => {
  goToSignup = async accountType => {
    props.setAccountType(accountType);
    await AsyncStorage.setItem('accountType', accountType);
    props.navigation.navigate('Signup');
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.typeText}> Who are you? </Text>
        <TouchableWithoutFeedback onPress={() => goToSignup('aspirant')}>
          <View style={styles.typeCard}>
            <View style={styles.cardImageSection}>
              <Image
                style={styles.image}
                source={require('../../assets/images/aspirant.png')}
              />
            </View>
            <View style={styles.cardDescriptionSection}>
              <Text style={styles.cardHeading}> Aspirant </Text>
              <Text style={styles.cardDescription}>
                {' '}
                looking to learn something new , referral or mentoring.{' '}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => goToSignup('expert')}>
          <View style={styles.typeCard}>
            <View style={styles.cardImageSection}>
              <Image
                style={styles.image}
                source={require('../../assets/images/expert.png')}
              />
            </View>
            <View style={styles.cardDescriptionSection}>
              <Text style={styles.cardHeading}> Expert </Text>
              <Text style={styles.cardDescription}>
                {' '}
                looking to share learning experience & guiding others.{' '}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.signinTextWrapper}>
          <Text style={styles.signinInfo}> Already Have an account ? </Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('SignIn')}>
            <Text style={styles.signinText}> SignIn </Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  signinText: {
    color: '#009fee',
    fontSize: 15,
    fontFamily: 'Roboto',
  },
  signinInfo: {
    color: 'gray',
    fontSize: 15,
    fontWeight: '400',
  },
  signinTextWrapper: {
    display: 'flex',
    width: '100%',

    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  subContainer: {
    fontWeight: '900',
    padding: '6%',
    fontFamily: 'Roboto',
    display: 'flex',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  typeText: {
    fontSize: 30,
    marginBottom: 50,
  },
  typeCard: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'rgb(248,248,248)',
    width: '100%',
    height: 120,
    borderRadius: 20,
    marginBottom: 30,
  },
  cardImageSection: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '30%',
  },
  cardDescriptionSection: {
    display: 'flex',
    width: '70%',
    padding: 10,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  image: {
    width: '50%',
    height: '50%',
  },
  cardHeading: {
    fontFamily: 'Roboto',
    fontSize: 18,
    fontWeight: '900',
  },
  cardDescription: {
    fontFamily: 'Roboto',
    fontWeight: '500',
    color: 'gray',
    fontSize: 18,
  },
});
const mapDispatchToProps = dispatch => {
  return {
    setAccountType: data => dispatch(setAccountType(data)),
  };
};
export default connect(
  null,
  mapDispatchToProps,
)(AccountTypeSignup);
