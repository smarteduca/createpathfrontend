import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  Alert,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import KeyboardShift from '../KeyboardShift/KeyboardShift';
import {
  setCountryCode,
  setPhoneNumber,
  forgotPassword,
  resetError,
  resetSuccess,
} from './store/actions/index';
import validator from 'validator';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../UI/Loader/Loader';
import data from '../../assets/data/countries';
const defaultFlag = data.filter(obj => obj.name === 'India')[0].flag;
const ForgotPassword = props => {
  const [flag, setFlag] = useState(defaultFlag);
  const [modalVisible, setModalVisible] = useState(false);
  const showModal = () => {
    setModalVisible(true);
  };
  const getCountry = async country => {
    const countryData = await data;
    try {
      const countryCode = await countryData.filter(
        obj => obj.name === country,
      )[0].dial_code;
      const countryFlag = await countryData.filter(
        obj => obj.name === country,
      )[0].flag;
      const countryName = await countryData
        .filter(obj => obj.name === country)[0]
        .name.toLowerCase();
      setFlag(countryFlag);
      props.setCountryCode(countryCode);

      await closeModal();
    } catch (err) {
      console.log(err);
    }
  };
  const closeModal = () => {
    setModalVisible(false);
  };
  const storePhoneNumber = async value => {
    try {
      await AsyncStorage.setItem('phoneNumber', value);
    } catch (err) {
      console.log(err);
    }
  };
  let isDisabled = false;
  const buttonStyles = [styles.button];
  if (
    !validator.isNumeric(props.phoneNumber) ||
    props.phoneNumber.length != 10
  ) {
    isDisabled = true;
    buttonStyles.push(styles.disablecss);
  }
  const countryData = data;
  if (props.success) {
    storePhoneNumber(props.countryCode + props.phoneNumber);
    props.resetSuccess();
    props.navigation.navigate('VerifyForgotOtp');
  }
  if (props.error && Platform.OS === 'android') {
    ToastAndroid.show(props.error, ToastAndroid.SHORT);
    props.resetError();
  }
  if (props.error && Platform.OS === 'ios') {
    Alert.alert(props.error);
    props.resetError();
  }
  return (
    <KeyboardShift>
      {() => (
        <SafeAreaView style={styles.container}>
          {props.ongoingRequest && <Loader />}
          <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}>
            <SafeAreaView style={styles.container}>
              <View style={styles.subContainer}>
                <Icon
                  name="times"
                  size={25}
                  style={styles.modalCloseIcon}
                  onPress={closeModal}
                />
                <FlatList
                  style={{height: '90%', width: '100%'}}
                  data={countryData}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() => getCountry(item.name)}>
                      <View
                        style={[
                          styles.countryStyle,
                          {
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-start',
                          },
                        ]}>
                        <Text style={{fontSize: 45}}>{item.flag}</Text>
                        <Text style={{fontSize: 20, color: '#fff'}}>
                          {item.name} ({item.dial_code})
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                />
              </View>
            </SafeAreaView>
          </Modal>
          <View style={styles.subContainer}>
            <Icon
              name="arrow-left"
              size={20}
              style={styles.backIcon}
              onPress={() => props.navigation.goBack()}
            />
            <Text style={styles.heading}> Forgot Password </Text>
            <Text style={styles.subHeading}>
              {' '}
              Reset code will be sent to you{' '}
            </Text>
            <View style={styles.phoneNumberView}>
              <TouchableWithoutFeedback onPress={() => showModal()}>
                <View style={styles.countryCodeInput}>
                  <Text style={styles.countryCodeText}>
                    {' '}
                    {flag + ' ' + props.countryCode}{' '}
                  </Text>
                  <Icon active name="caret-down" style={styles.iconArrow} />
                </View>
              </TouchableWithoutFeedback>
              <TextInput
                keyboardType="phone-pad"
                placeholderTextColor="rgb(227,227,227)"
                style={styles.numberInput}
                placeholder="Phone Number"
                value={props.phoneNumber}
                onChangeText={text => props.setPhoneNumber(text)}
              />
            </View>
            <TouchableOpacity
              disabled={isDisabled}
              style={buttonStyles}
              onPress={() =>
                props.forgotPassword(props.countryCode + props.phoneNumber)
              }>
              <Text style={styles.buttonText}> Send Code </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      )}
    </KeyboardShift>
  );
};
const styles = StyleSheet.create({
  countryStyle: {
    flex: 1,
    backgroundColor: '#009fee',
    borderTopColor: '#fff',
    borderTopWidth: 1,
    padding: 12,
  },
  numberInput: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '65%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  iconArrow: {
    fontSize: 20,
    alignSelf: 'center',
  },
  countryCodeInput: {
    display: 'flex',
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginBottom: 30,
    paddingBottom: 20,
  },
  countryCodeText: {
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  phoneNumberView: {
    marginTop: 30,
    marginBottom: 30,
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  modalCloseIcon: {
    alignSelf: 'flex-end',
    height: '10%',
  },
  disablecss: {
    opacity: 0.4,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',

    alignItems: 'flex-start',
    padding: '6%',
  },
  backIcon: {
    marginTop: '5%',
    marginBottom: '5%',
  },
  heading: {
    marginTop: 30,
    fontFamily: 'Roboto',
    fontWeight: '900',
    fontSize: 28,
  },
  subHeading: {
    marginTop: '9%',
    fontFamily: 'Roboto',
    fontWeight: '600',
    color: 'rgb(122,122,122)',
    fontSize: 15,
  },

  button: {
    marginTop: 30,
    display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '100%',
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto',
  },
});
const mapStateToProps = state => {
  return {
    ongoingRequest: state.forgotPassword.ongoingRequest,
    phoneNumber: state.forgotPassword.phoneNumber,
    countryCode: state.forgotPassword.countryCode,
    success: state.forgotPassword.success,
    error: state.forgotPassword.error,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setCountryCode: data => dispatch(setCountryCode(data)),
    setPhoneNumber: data => dispatch(setPhoneNumber(data)),
    forgotPassword: data => dispatch(forgotPassword(data)),
    resetError: () => dispatch(resetError()),
    resetSuccess: () => dispatch(resetSuccess()),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);
