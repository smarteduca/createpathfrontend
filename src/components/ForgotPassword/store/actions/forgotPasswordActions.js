import * as actions from './actionTypes';
import env from '../../../../config/environment';
export const setPhoneNumber = value => {
  return {
    type: actions.SET_FORGOT_PASSWORD_PHONE_NUMBER,
    value,
  };
};
export const setCountryCode = value => {
  return {
    type: actions.SET_FORGOT_PASSWORD_COUNTRY_CODE,
    value,
  };
};
export const resetError = () => {
  return {
    type: actions.RESET_FORGOT_PASSWORD_ERROR,
  };
};
const forgotPasswordSuccess = () => {
  return {
    type: actions.FORGOT_PASSWORD_SUCCESS,
  };
};
const forgotPasswordError = error => {
  return {
    type: actions.FORGOT_PASSWORD_ERROR,
    error: error,
  };
};
const setOngoingRequest = value => {
  return {
    type: actions.SET_FORGOT_PASSWORD_ONGOING_REQUEST,
    value,
  };
};
export const resetSuccess = () => {
  return {
    type: actions.RESET_FORGOT_PASSWORD_SUCCESS,
  };
};
export const forgotPassword = phoneNumber => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'verify/sendForgotPasswordCode', {
      method: 'POST',
      body: JSON.stringify({phoneNumber}),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(forgotPasswordSuccess());
        } else {
          dispatch(forgotPasswordError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(signupInfoError('Something went wrong'));
      });
  };
};
