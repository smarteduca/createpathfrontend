import * as actions from '../actions/actionTypes';
const initialState = {
  success: false,
  error: null,
  ongoingRequest: false,
  phoneNumber: '',
  countryCode: '+91',
};
const forgotPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.RESET_FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        success: false,
      };
    case actions.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        success: true,
      };
    case actions.FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actions.SET_FORGOT_PASSWORD_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.RESET_FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        error: null,
      };
    case actions.SET_FORGOT_PASSWORD_COUNTRY_CODE:
      return {
        ...state,
        countryCode: action.value,
      };
    case actions.SET_FORGOT_PASSWORD_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.value,
      };
    default:
      return state;
  }
};
export default forgotPasswordReducer;
