import React from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';
const Loader = props => {
  return (
    <View style={styles.ongoingRequestView}>
      <ActivityIndicator size="large" color="#009fe" />
    </View>
  );
};
const styles = StyleSheet.create({
  ongoingRequestView: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    position: 'absolute',
    zIndex: 100,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
});
export default Loader;
