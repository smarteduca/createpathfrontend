import React, {useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  resetError,
  resetSuccess,
  verifyOtp,
  resetResendCodeSuccess,
  resendCodeVerifyOtp,
} from '../VerifyOtp/store/actions/index';
import Loader from '../UI/Loader/Loader';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import validator from 'validator';
const VerifyForgotOtp = props => {
  const [code, setCode] = useState('');
  let isDisabled = false;
  const verifyButtonStyles = [styles.verifyButton];
  const resendCode = async () => {
    const phoneNumber = await AsyncStorage.getItem('phoneNumber');
    const data = {
      phoneNumber,
    };
    props.resendCodeVerifyOtp(data);
  };
  const goToSetPassword = async () => {
    const phoneNumber = await AsyncStorage.getItem('phoneNumber');
    const data = {
      phoneNumber,
      code,
    };
    props.verifyOtp(data);
  };
  if (!validator.isNumeric(code) || code.length != 6) {
    isDisabled = true;
    verifyButtonStyles.push(styles.disablecss);
  }
  if (props.success) {
    props.resetSuccess();
    props.navigation.navigate('ResetPassword');
  }
  if (props.error && Platform.OS === 'android') {
    ToastAndroid.show(props.error, ToastAndroid.SHORT);
    props.resetError();
  }
  if (props.error && Platform.OS === 'ios') {
    Alert.alert(props.error);
    props.resetError();
  }
  if (props.resendCodeSuccess && Platform.OS === 'android') {
    ToastAndroid.show('Code Sent Successfully!', ToastAndroid.SHORT);
    props.resetResendCodeSuccess();
  }
  if (props.resendCodeSuccess && Platform.OS === 'ios') {
    Alert.alert('Code Sent Successfully!');
    props.resetResendCodeSuccess();
  }
  return (
    <SafeAreaView style={styles.container}>
      {props.ongoingRequest && <Loader />}
      <View style={styles.subContainer}>
        <Icon
          name="arrow-left"
          size={20}
          style={styles.backIcon}
          onPress={() => props.navigation.goBack()}
        />
        <Text style={styles.heading}> We Have Sent You an OTP </Text>

        <View style={styles.enterOtpView}>
          <TextInput
            style={styles.input}
            onChangeText={text => setCode(text)}
            keyboardType="number-pad"
          />
          <TouchableOpacity
            disabled={isDisabled}
            style={verifyButtonStyles}
            onPress={() => goToSetPassword()}>
            <Icon style={styles.rightIcon} name="chevron-right" size={30} />
          </TouchableOpacity>
        </View>
        <View style={styles.footer}>
          <Icon
            name="redo-alt"
            size={18}
            color="rgb(151,155,163)"
            style={styles.refreshIcon}
            onPress={() => resendCode()}
          />
          <Text style={styles.resendCodeText}> Resend Code </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  verifyButton: {
    marginLeft: '12%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '25%',
    backgroundColor: 'rgb(51,85,222)',
    borderRadius: 20,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    position: 'relative',
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',

    alignItems: 'flex-start',
    padding: '6%',
  },
  backIcon: {
    marginTop: '5%',
    marginBottom: '5%',
  },
  heading: {
    fontFamily: 'Roboto',
    fontWeight: '900',
    fontSize: 28,
  },
  enterOtpView: {
    marginTop: '15%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',

    width: '100%',
  },
  input: {
    borderBottomWidth: 3,
    width: '40%',
    fontFamily: 'Roboto',
    paddingBottom: 20,
    fontSize: 40,
    textAlign: 'center',
  },
  disablecss: {
    opacity: 0.4,
  },
  resendCodeText: {
    marginLeft: 20,
    color: 'rgb(51,85,222)',
    fontFamily: 'Roboto',
    fontSize: 18,
  },
  refreshIcon: {
    marginLeft: 20,

    backgroundColor: 'rgb(227,227,227)',
    padding: 8,
    borderRadius: 10,
  },
  rightIcon: {
    color: '#fff',
  },
  footer: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '15%',
    width: '100%',
    borderTopWidth: 2,
    borderTopColor: 'rgb(227,227,227)',
    bottom: 0,
    left: 0,
    marginLeft: '6%',
  },
});
const mapStateToProps = state => {
  return {
    ongoingRequest: state.verifyOtp.ongoingRequest,
    success: state.verifyOtp.success,
    error: state.verifyOtp.error,
    resendCodeSuccess: state.verifyOtp.resendCodeSuccess,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetError: data => dispatch(resetError(data)),
    resetSuccess: data => dispatch(resetSuccess(data)),
    verifyOtp: data => dispatch(verifyOtp(data)),
    resetResendCodeSuccess: () => dispatch(resetResendCodeSuccess()),
    resendCodeVerifyOtp: data => dispatch(resendCodeVerifyOtp(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerifyForgotOtp);
