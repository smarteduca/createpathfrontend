import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
const Header = props => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.headerIcon}
        source={require('../../../assets/images/IntroScreenIcon.png')}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: '15%',
  },
  headerIcon: {
    marginLeft: '12%',
    width: '50%',
    height: '50%',
  },
});
export default Header;
