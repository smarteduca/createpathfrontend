import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
const Footer = props => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('AccountType')}
        style={styles.button}>
        <Text style={styles.buttonText}> Continue </Text>
      </TouchableOpacity>
      <View style={styles.signinTextWrapper}>
        <Text style={styles.signinInfo}> Already Have an account ? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
          <Text style={styles.signinText}> SignIn </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    height: '15%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  button: {
    display: 'flex',
    padding: 15,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '70%',
  },
  buttonText: {
    color: '#fff',
    textTransform: 'uppercase',
  },
  signinTextWrapper: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center',
  },
  signinInfo: {
    color: 'gray',
    fontSize: 15,
    fontWeight: '400',
  },
  signinText: {
    color: '#009fee',
  },
});
export default Footer;
