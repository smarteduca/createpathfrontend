import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import Swiper from 'react-native-swiper';
const Slider = props => {
  return (
    <View style={styles.container}>
      <Swiper activeDotStyle={{width: 20}}>
        <View style={styles.slide}>
          <Image
            style={styles.headerIcon}
            source={require('../../../assets/images/rocket.png')}
          />
          <Text style={styles.slideHeading}>
            {' '}
            Accelerate towards your Goal{' '}
          </Text>
          <Text style={styles.slideSubHeading}>
            {' '}
            Achieve Your Goal by following foot steps suggested by Successful
            People in your field.{' '}
          </Text>
        </View>
        <View style={styles.slide}>
          <Image
            style={styles.headerIcon}
            source={require('../../../assets/images/mentor.png')}
          />
          <Text style={styles.slideHeading}> Schedule Mentor Sessions </Text>
          <Text style={styles.slideSubHeading}>
            Got Stuck ? Schedule a mentor session to follow your path to
            greatness.
          </Text>
        </View>
        <View style={styles.slide}>
          <Image
            style={styles.headerIcon}
            source={require('../../../assets/images/referal.png')}
          />
          <Text style={styles.slideHeading}> Get A Referal </Text>
          <Text style={styles.slideSubHeading}>
            After completing suggested path you can ask for a reference from
            your mentor.
          </Text>
        </View>
        <View style={styles.slide}>
          <Image
            style={styles.headerIcon}
            source={require('../../../assets/images/beamentor.png')}
          />
          <Text style={styles.slideHeading}> Become A Mentor </Text>
          <Text style={styles.slideSubHeading}>
            Get a chance to mentor aspirants looking to enter your field by
            simply designing path you followed.
          </Text>
        </View>
        <View style={styles.slide}>
          <Image
            style={styles.headerIcon}
            source={require('../../../assets/images/money.png')}
          />
          <Text style={styles.slideHeading}>
            {' '}
            Lifetime Earning & Referal Bouns!{' '}
          </Text>
          <Text style={styles.slideSubHeading}>
            Get a lifetime earning opportunity & referal bonus by helping
            aspirants and refering them in case they are fit for your company.
          </Text>
        </View>
      </Swiper>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    height: '70%',
  },
  slide: {
    display: 'flex',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  slideHeading: {
    color: '#009fee',
    fontSize: 25,
    fontWeight: '700',
  },
  slideSubHeading: {
    width: '80%',
    color: '#333',
    fontSize: 20,
    fontWeight: '300',
    fontStyle: 'italic',
  },
  headerIcon: {
    width: 200,
    height: 200,
    borderRadius: 100,
  },
});

export default Slider;
