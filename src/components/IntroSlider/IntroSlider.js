import React, {PureComponent} from 'react';
import {View, Text, StyleSheet, Platform, SafeAreaView} from 'react-native';
import Header from './Header/Header';
import Slider from './Slider/Slider';
import Footer from './Footer/Footer';
class IntroSlider extends PureComponent {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header />
        <Slider />
        <Footer />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
export default IntroSlider;
