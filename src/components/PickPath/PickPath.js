import React, {PureComponent} from 'react';
import {
  Text,
  SafeAreaView,
  StyleSheet,
  View,
  ScrollView,
  TouchableWithoutFeedback,
  Platform,
  ToastAndroid,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {resetError, loadPaths, proceedToDashboard} from './store/actions/index';
import Loader from '../UI/Loader/Loader';
class PickPath extends PureComponent {
  state = {
    accountType: '',

    selectedPaths: [],
  };
  async UNSAFE_componentWillMount() {
    console.log('component will mount');
    this.props.loadPaths();
    try {
      const accountType = await AsyncStorage.getItem('accountType');

      this.setState({accountType: accountType});
    } catch (err) {
      console.log(err);
    }
  }
  getHeading = () => {
    const accountType = this.state.accountType;
    const headingText =
      accountType === 'aspirant' ? (
        <Text style={styles.heading}> Choose Paths that interests you </Text>
      ) : (
        <Text style={styles.heading}> Choose Paths you have worked on </Text>
      );
    return headingText;
  };
  toggleSelection = id => {
    const selectedPaths = [...this.state.selectedPaths];
    const index = selectedPaths.indexOf(id);
    if (index > -1) {
      selectedPaths.splice(index, 1);
    } else {
      selectedPaths.push(id);
    }

    this.setState({selectedPaths});
  };
  goToDashboard = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log('tokennnn', token);
      const selected_paths = this.state.selectedPaths;
      const data = {
        token,
        selected_paths,
      };
      this.props.proceedToDashboard(data);
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    const accountType = this.state.accountType;
    let isDisabled = false;
    let proceedIconStyles = [styles.proceedIcon];
    if (this.state.selectedPaths.length === 0) {
      isDisabled = true;
      proceedIconStyles = [styles.proceedIcon, styles.disablecss];
    }
    if (this.props.success && accountType === 'aspirant') {
      this.props.navigation.navigate('AspirantDashboard');
    }
    if (this.props.success && accountType === 'expert') {
      this.props.navigation.navigate('ExpertDashboard');
    }
    if (this.props.error && Platform.OS === 'android') {
      ToastAndroid.show(this.props.error, ToastAndroid.SHORT);
      this.props.resetError();
    }
    if (this.props.error && Platform.OS === 'ios') {
      Alert.alert(this.props.error);
      this.props.resetError();
    }
    return (
      <SafeAreaView style={styles.container}>
        {this.props.ongoingRequest && <Loader />}
        <ScrollView
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
          style={styles.subContainer}>
          {this.getHeading()}
          <Text style={styles.subHeading}> Select atleast one path</Text>
          <View style={styles.pathsContainer}>
            {this.props.paths.map((path, index) => {
              let pathSelected = false;
              let itemStyles = styles.pathItem;
              let itemText = styles.pathText;
              const selectedPaths = this.state.selectedPaths;
              const pathIndex = selectedPaths.indexOf(path._id);

              if (pathIndex > -1) {
                pathSelected = true;
                itemStyles = styles.selectedItem;
                itemText = styles.selectedText;
              }

              return (
                <TouchableWithoutFeedback
                  onPress={() => this.toggleSelection(path._id)}>
                  <View key={index} style={itemStyles}>
                    <Text style={itemText}> {path.name} </Text>
                  </View>
                </TouchableWithoutFeedback>
              );
            })}
          </View>
          <TouchableWithoutFeedback
            onPress={() => this.goToDashboard()}
            disabled={isDisabled}>
            <View style={proceedIconStyles}>
              <Icon name="arrow-right" color="white" size={30} />
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  disablecss: {
    opacity: 0.4,
  },
  proceedIcon: {
    padding: 13,
    marginTop: '8%',
    backgroundColor: '#009fee',
    borderRadius: 50,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',

    padding: '6%',
    height: '100%',
    width: '100%',
  },
  heading: {
    fontStyle: 'italic',
    fontFamily: 'Roboto',
    fontSize: 22,
  },
  subHeading: {
    margin: 10,
    fontStyle: 'italic',
    fontFamily: 'Roboto',
    fontSize: 15,
  },
  pathsContainer: {
    justifyContent: 'flex-start',

    marginTop: '2%',
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
  },
  pathItem: {
    textAlign: 'center',
    marginBottom: '2%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '3%',

    padding: 10,
    borderWidth: 2,
    borderColor: '#009fee',
  },
  pathText: {
    color: '#009fee',
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },
  selectedItem: {
    textAlign: 'center',
    marginBottom: '2%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '3%',
    backgroundColor: '#009fee',

    padding: 10,
    borderWidth: 2,
    borderColor: '#009fee',
  },
  selectedText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },
});
const mapStateToProps = state => {
  return {
    success: state.pickPath.success,
    error: state.pickPath.error,
    ongoingRequest: state.pickPath.ongoingRequest,
    paths: state.pickPath.paths,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetError: () => dispatch(resetError()),
    loadPaths: () => dispatch(loadPaths()),
    proceedToDashboard: data => dispatch(proceedToDashboard(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickPath);
