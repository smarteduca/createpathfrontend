import * as actions from './actionTypes';
import env from '../../../../config/environment';

const pickPathSuccess = () => {
  return {
    type: actions.PICK_PATH_SUCCESS,
  };
};
const pickPathError = error => {
  return {
    type: actions.PICK_PATH_ERROR,
    error,
  };
};
export const resetError = () => {
  return {
    type: actions.PICK_PATH_RESET_ERROR,
  };
};
const setOngoingRequest = value => {
  return {
    type: actions.PICK_PATH_SET_ONGOING_REQUEST,
    value,
  };
};
const loadPathsSuccess = value => {
  return {
    type: actions.PICK_PATH_LOAD_PATHS_SUCCESS,
    value,
  };
};
export const loadPaths = () => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'path/all_paths', {
      headers: {
        accept: 'application/json',
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(loadPathsSuccess(result.paths));
        } else {
          dispatch(pickPathError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(pickPathError('Something went wrong'));
      });
  };
};
export const proceedToDashboard = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'path/add_path', {
      method: 'POST',
      body: JSON.stringify({selected_paths: data.selected_paths}),
      headers: {
        Authorization: `Bearer ${data.token}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => res.json())
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.success === true) {
          dispatch(pickPathSuccess());
        } else {
          dispatch(pickPathError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(pickPathError('Something went wrong'));
      });
  };
};
