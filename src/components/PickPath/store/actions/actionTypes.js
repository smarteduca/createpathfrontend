export const PICK_PATH_SUCCESS = 'PICK_PATH_SUCCESS';
export const PICK_PATH_ERROR = 'PICK_PATH_ERROR';
export const PICK_PATH_SET_ONGOING_REQUEST = 'PICK_PATH_SET_ONGOING_REQUEST';
export const PICK_PATH_RESET_ERROR = 'PICK_PATH_RESET_ERROR';
export const PICK_PATH_LOAD_PATHS_SUCCESS = 'PICK_PATH_LOAD_PATHS_SUCCESS';
