import * as actions from '../actions/actionTypes';
const initialState = {
  ongoingRequest: false,
  success: false,
  error: null,
  paths: [],
};
const pickPathReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.PICK_PATH_LOAD_PATHS_SUCCESS:
      return {
        ...state,
        paths: action.value,
      };
    case actions.PICK_PATH_SUCCESS:
      return {
        ...state,
        success: true,
      };
    case actions.PICK_PATH_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actions.PICK_PATH_SET_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.PICK_PATH_RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
export default pickPathReducer;
