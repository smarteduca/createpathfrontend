import React, {PureComponent} from 'react';
import {
  ScrollView,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Modal,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  FlatList,
  ToastAndroid,
  Alert,
} from 'react-native';
import data from '../../assets/data/statecity.json';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Loader from '../UI/Loader/Loader';
import {signup, resetSignupInfoError} from './store/actions/index';
import AsyncStorage from '@react-native-community/async-storage';
import validator from 'validator';

class SignupInfo extends PureComponent {
  state = {
    citymodalvisible: false,
    statesmodalvisible: false,
    states: [],
    cities: [],
    filteredStates: [],
    filteredCities: [],
    showPassword: false,
    fullName: '',
    password: '',
    countryState: '',
    countryCity: '',
    gender: 'male',

    genderOptions: ['male', 'female'],
  };

  async componentDidMount() {
    console.log('Signup Info mounted');

    await this.loadStates();
    console.log('states loaded');
  }

  openStatesModal = () => {
    this.setState({statesmodalvisible: true});
  };
  openCountryModal = () => {
    this.setState({citymodalvisible: true});
  };
  closeStatesModal = () => {
    this.setState({statesmodalvisible: false});
  };
  loadStates = async () => {
    const loadData = await data;
    try {
      const country = this.props.country;
      const states = await loadData.filter(
        obj => obj.name.toLowerCase() === country,
      )[0].states;
      this.setState({states: states});
      this.setState({filteredStates: states});
    } catch (err) {
      console.log('error', err);
    }
  };
  toggleShowPassword = () => {
    console.log('toggle show password called');
    const showPassword = this.state.showPassword;
    this.setState({showPassword: !showPassword});
  };
  displayGenderOptions = () => {
    return this.state.genderOptions.map((option, index) => {
      if (option === this.state.gender) {
        return (
          <TouchableWithoutFeedback
            key={index}
            onPress={() => this.setState({gender: option})}>
            <View style={styles.activeOption}>
              <Text style={styles.activeOptionText}> {option} </Text>
            </View>
          </TouchableWithoutFeedback>
        );
      } else {
        return (
          <TouchableWithoutFeedback
            key={index}
            onPress={() => this.setState({gender: option})}>
            <View key={index} style={styles.genderOption}>
              <Text style={styles.optionText}> {option} </Text>
            </View>
          </TouchableWithoutFeedback>
        );
      }
    });
  };
  filterStates = text => {
    const filteredStates = this.state.states.filter(
      state => state.name.toLowerCase().indexOf(text.toLowerCase()) >= 0,
    );
    this.setState({filteredStates: filteredStates});
  };
  selectState = selectedState => {
    this.setState({countryState: selectedState, statesmodalvisible: false});
    const cities = this.state.states.filter(
      state => state.name.toLowerCase() === selectedState.toLowerCase(),
    )[0].cities;
    console.log('cities', cities);
    this.setState({cities: cities, filteredCities: cities});
  };
  selectCity = city => {
    this.setState({countryCity: city, citymodalvisible: false});
  };
  filterCities = text => {
    const filteredCities = this.state.cities.filter(
      city => city.name.toLowerCase().indexOf(text.toLowerCase()) >= 0,
    );
    this.setState({filteredCities: filteredCities});
  };
  closeCitiesModal = () => {
    this.setState({citymodalvisible: false});
  };
  inputChangeHandler = (propertyName, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [propertyName]: value,
      };
    });
  };
  signup = async () => {
    console.log('signup function called');
    const phoneNumber = this.props.countryCode + this.props.phoneNumber;
    const email = this.props.email.toLowerCase();
    const password = this.state.password;
    const name = this.state.fullName;
    const accountType = await AsyncStorage.getItem('accountType');
    const gender = this.state.gender;
    const state = this.state.countryState;
    const city = this.state.countryCity;
    const country = this.props.country;
    this.props.signup({
      phoneNumber,
      email,
      name,
      accountType,
      gender,
      state,
      city,
      country,
      password,
    });
  };
  setToken = async () => {
    try {
      await AsyncStorage.setItem('token', this.props.token);
      this.props.navigation.navigate('PickPath');
    } catch (err) {
      console.log(err);
    }
  };
  getAccountType = async () => {
    try {
      const accountType = await AsyncStorage.getItem('accountType');
      return accountType;
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    let isDisabled = false;
    const buttonStyles = [styles.signupButton];
    if (
      this.state.countryCity === '' ||
      this.state.countryState === '' ||
      validator.isEmpty(this.state.fullName) ||
      !validator.isLength(this.state.password, {
        min: 8,
      })
    ) {
      isDisabled = true;
      buttonStyles.push(styles.disablecss);
    }
    if (this.props.success) {
      this.setToken();
      // this.props.navigation.navigate('PickPath');
    }
    // if (this.props.success) {
    //   this.getAccountType().then(value => {
    //     if (value === 'aspirant') {
    //       this.setToken();
    //       this.props.navigation.navigate('AspirantDashboard');
    //     } else {
    //       this.setToken();
    //       this.props.navigation.navigate('ExpertDashboard');
    //     }
    //   });
    // }

    if (this.props.error && Platform.OS === 'android') {
      ToastAndroid.show(this.props.error, ToastAndroid.SHORT);
      this.props.resetSignupInfoError();
    }
    if (this.props.error && Platform.OS === 'ios') {
      Alert.alert(this.props.error);
      this.props.resetSignupInfoError();
    }
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.subContainer}>
          {this.props.ongoingRequest && <Loader />}
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.citymodalvisible}>
            <SafeAreaView style={styles.modalContainer}>
              <View style={styles.subContainer}>
                <View style={styles.searchableView}>
                  <TextInput
                    onChangeText={text => this.filterCities(text)}
                    placeholderTextColor="rgb(227,227,227)"
                    style={styles.searchInput}
                    placeholder="Search Cities"
                  />
                  <Icon
                    name="times"
                    size={30}
                    style={styles.modalCloseIcon}
                    onPress={() => this.closeCitiesModal()}
                  />
                </View>
                <FlatList
                  getItemLayout={(data, index) => ({
                    length: 60,
                    offset: 60 * index,
                    index,
                  })}
                  style={styles.modalList}
                  data={this.state.filteredCities}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() => this.selectCity(item.name)}>
                      <View style={styles.listView}>
                        <Text style={styles.listText}> {item.name} </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  keyExtractor={item => item.name}
                />
              </View>
            </SafeAreaView>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.statesmodalvisible}>
            <SafeAreaView style={styles.modalContainer}>
              <View style={styles.subContainer}>
                <View style={styles.searchableView}>
                  <TextInput
                    onChangeText={text => this.filterStates(text)}
                    placeholderTextColor="rgb(227,227,227)"
                    style={styles.searchInput}
                    placeholder="Search States"
                  />
                  <Icon
                    name="times"
                    size={30}
                    style={styles.modalCloseIcon}
                    onPress={() => this.closeStatesModal()}
                  />
                </View>
                <FlatList
                  getItemLayout={(data, index) => ({
                    length: 60,
                    offset: 60 * index,
                    index,
                  })}
                  style={styles.modalList}
                  data={this.state.filteredStates}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() => this.selectState(item.name)}>
                      <View style={styles.listView}>
                        <Text style={styles.listText}> {item.name} </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  keyExtractor={item => item.name}
                />
              </View>
            </SafeAreaView>
          </Modal>

          <View style={styles.inputWrapper}>
            <Icon
              style={styles.inputIcon}
              name="portrait"
              size={30}
              color="rgb(227,227,227)"
            />
            <TextInput
              value={this.state.fullName}
              onChangeText={text => this.inputChangeHandler('fullName', text)}
              placeholderTextColor="rgb(227,227,227)"
              style={styles.input}
              placeholder="Full Name"
            />
          </View>
          <View style={styles.inputWrapper}>
            <Icon
              onPress={this.toggleShowPassword}
              style={styles.inputIcon}
              name={this.state.showPassword ? 'eye' : 'eye-slash'}
              size={25}
              color="rgb(227,227,227)"
            />
            <TextInput
              value={this.state.password}
              onChangeText={text => this.inputChangeHandler('password', text)}
              secureTextEntry={!this.state.showPassword}
              textContentType="password"
              placeholderTextColor="rgb(227,227,227)"
              style={styles.passwordInput}
              placeholder="Set Password"
            />
            <Text style={styles.passwordConditionText}>
              {' '}
              Password must contain at least 8 characters{' '}
            </Text>
          </View>
          <TouchableWithoutFeedback onPress={() => this.openStatesModal()}>
            <View style={styles.locationInput}>
              {this.state.countryState != '' ? (
                <Text style={styles.selectedText}>
                  {' '}
                  {this.state.countryState}{' '}
                </Text>
              ) : (
                <Text style={styles.locationText}> State </Text>
              )}
              <Icon
                name="map-marker"
                color="rgb(227,227,227)"
                style={styles.locationIcon}
              />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.openCountryModal()}>
            <View style={styles.cityInput}>
              {this.state.countryCity != '' ? (
                <Text style={styles.selectedText}>
                  {' '}
                  {this.state.countryCity}{' '}
                </Text>
              ) : (
                <Text style={styles.locationText}> City </Text>
              )}
              <Icon
                name="map-marker"
                color="rgb(227,227,227)"
                style={styles.locationIcon}
              />
            </View>
          </TouchableWithoutFeedback>
          <Text style={styles.genderText}> Gender </Text>
          <View style={styles.genderOptionsView}>
            {this.displayGenderOptions()}
          </View>
          <TouchableOpacity
            disabled={isDisabled}
            style={buttonStyles}
            onPress={this.signup}>
            <Text style={styles.signupButtonText}> Submit </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  disablecss: {
    opacity: 0.4,
  },
  listText: {
    fontFamily: 'Roboto',
    fontSize: 20,
    color: 'rgb(120,120,120)',
    fontWeight: '700',
  },
  listView: {
    display: 'flex',
    justifyContent: 'center',

    height: 60,
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
  },
  modalList: {
    flexGrow: 0,
    width: '100%',
    height: '75%',
    marginTop: '15%',
  },
  modalContainer: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
  },

  subContainer: {
    padding: '6%',
    display: 'flex',
    height: '100%',
    width: '100%',
  },
  inputWrapper: {
    display: 'flex',
    width: '100%',
    position: 'relative',
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  passwordInput: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 10,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  inputIcon: {
    position: 'absolute',
    right: 0,
    bottom: 50,
    zIndex: 1,
  },
  passwordConditionText: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: 'rgb(186,186,186)',
  },
  locationInput: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginTop: 60,
    marginBottom: 30,
    paddingBottom: 20,
  },
  cityInput: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginTop: 30,
    marginBottom: 30,
    paddingBottom: 20,
  },
  locationText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(227,227,227)',
  },
  selectedText: {
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  locationIcon: {
    fontSize: 20,
    alignSelf: 'center',
  },
  genderText: {
    fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: '900',
    marginBottom: 30,
  },
  genderOptionsView: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  genderOption: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    height: 70,
    width: '45%',
    backgroundColor: 'rgb(248,248,248)',
  },
  optionText: {
    fontFamily: 'Roboto',
    textTransform: 'capitalize',
    fontSize: 20,
    color: 'rgb(96,96,96)',
  },
  activeOption: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    height: 70,
    width: '45%',
    backgroundColor: 'rgb(248,248,248)',
    borderWidth: 2,
    borderColor: '#009fee',
  },
  activeOptionText: {
    fontFamily: 'Roboto',
    textTransform: 'capitalize',
    fontSize: 20,
    color: '#009fee',
  },
  signupButton: {
    display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '100%',
    marginTop: 30,
    marginBottom: 30,
  },
  signupButtonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  searchableView: {
    display: 'flex',
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  searchInput: {
    paddingBottom: 20,

    fontFamily: 'Roboto',
    fontSize: 20,
    width: '70%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  modalCloseIcon: {
    alignSelf: 'center',
  },
});
const mapStateToProps = state => {
  return {
    ongoingRequest: state.signupInfo.ongoingRequest,
    phoneNumber: state.signup.phoneNumber,
    countryCode: state.signup.countryCode,
    email: state.signup.email,
    country: state.signup.country,
    success: state.signupInfo.success,
    error: state.signupInfo.error,
    accountType: state.signupInfo.accountType,
    token: state.signupInfo.token,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetSignupInfoError: () => dispatch(resetSignupInfoError()),
    signup: data => dispatch(signup(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignupInfo);
