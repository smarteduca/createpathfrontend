import * as actions from '../actions/actionTypes';
import data from '../../../../assets/data/countries';
const initialState = {
  ongoingRequest: false,
  token: null,
  success: false,
  error: null,
};
const signupInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SIGNUP_INFO_RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    case actions.SIGNUP_INFO_SUCCESS:
      return {
        ...state,
        token: action.data.token,

        success: true,
      };
    case actions.SIGNUP_INFO_ERROR:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};
export default signupInfoReducer;
