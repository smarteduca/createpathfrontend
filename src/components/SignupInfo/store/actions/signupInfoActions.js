import * as actions from './actionTypes';
import env from '../../../../config/environment';
const setOngoingRequest = value => {
  return {
    type: actions.SIGNUP_INFO_SET_ONGOING_REQUEST,
    value,
  };
};
const signupInfoSuccess = data => {
  return {
    type: actions.SIGNUP_INFO_SUCCESS,
    data: data,
  };
};
const signupInfoError = error => {
  return {
    type: actions.SIGNUP_INFO_ERROR,
    error: error,
  };
};
export const signup = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'auth/signup', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOngoingRequest(false));
        console.log(result);
        if (result.success === true) {
          console.log('Signup successful');
          const data = {
            token: result.token,
          };
          dispatch(signupInfoSuccess(data));
        } else {
          dispatch(signupInfoError('Something went wrong'));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(signupInfoError('Provide full Details'));
      });
  };
};
export const resetSignupInfoError = () => {
  return {
    type: actions.SIGNUP_INFO_RESET_ERROR,
  };
};
