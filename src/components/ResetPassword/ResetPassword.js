import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  ToastAndroid,
  Alert,
} from 'react-native';
import KeyBoardShift from '../KeyboardShift/KeyboardShift';
import Icon from 'react-native-vector-icons/FontAwesome5';
import validator from 'validator';
import {connect} from 'react-redux';
import {resetError, resetPassword} from './store/actions/index';
import Loader from '../UI/Loader/Loader';
import AsyncStorage from '@react-native-community/async-storage';
const ResetPassword = props => {
  const [showPassword, setShowPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  let isDisabled = false;
  const resetButtonStyles = [styles.resetButton];
  if (!validator.equals(password, confirmPassword) || password.length < 8) {
    isDisabled = true;
    resetButtonStyles.push(styles.disablecss);
  }
  const toggleShowPassword = () => {
    console.log('toggle Show Password');
    setShowPassword(!showPassword);
  };
  const reset = async () => {
    const phoneNumber = await AsyncStorage.getItem('phoneNumber');
    const data = {
      phoneNumber,
      newPassword: password,
    };
    props.resetPassword(data);
  };
  if (props.error && Platform.OS === 'android') {
    ToastAndroid.show(props.error, ToastAndroid.SHORT);
    props.resetError();
  }
  if (props.error && Platform.OS === 'ios') {
    Alert.alert(props.error);
    props.resetError();
  }
  if (props.success && props.accountType === 'aspirant') {
    props.navigation.navigate('AspirantDashboard');
  }
  if (props.success && props.accountType === 'expert') {
    props.navigation.navigate('ExpertDashboard');
  }
  return (
    <KeyBoardShift>
      {() => (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView style={styles.container}>
            {props.ongoingRequest && <Loader />}
            <View style={styles.subContainer}>
              <Icon
                name="arrow-left"
                size={20}
                style={styles.backIcon}
                onPress={() => props.navigation.goBack()}
              />
              <Text style={styles.heading}> Reset Password </Text>
              <View style={styles.passwordInput}>
                <Icon
                  onPress={toggleShowPassword}
                  style={styles.passwordIcon}
                  name={showPassword ? 'eye' : 'eye-slash'}
                  size={20}
                  color="rgb(227,227,227)"
                />
                <TextInput
                  value={password}
                  onChangeText={text => setPassword(text)}
                  secureTextEntry={!showPassword}
                  textContentType="password"
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Set Password"
                />
              </View>
              <TextInput
                value={confirmPassword}
                onChangeText={text => setConfirmPassword(text)}
                secureTextEntry={!showPassword}
                textContentType="password"
                placeholderTextColor="rgb(227,227,227)"
                style={styles.input}
                placeholder="Confirm Password"
              />

              <TouchableOpacity
                disabled={isDisabled}
                style={resetButtonStyles}
                onPress={() => reset()}>
                <Text style={styles.resetButtonText}> Reset </Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      )}
    </KeyBoardShift>
  );
};
const styles = StyleSheet.create({
  disablecss: {
    opacity: 0.4,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',

    alignItems: 'flex-start',
    padding: '6%',
  },
  backIcon: {
    marginTop: '5%',
    marginBottom: '5%',
  },
  heading: {
    fontFamily: 'Roboto',
    marginTop: 40,
    marginBottom: 40,
    fontSize: 25,
  },
  passwordInput: {
    display: 'flex',
    width: '100%',
    position: 'relative',
  },
  passwordIcon: {
    position: 'absolute',
    right: 0,
    bottom: 50,
    zIndex: 1,
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  resetButtonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  resetButton: {
    display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '100%',
  },
});
const mapStateToProps = state => {
  return {
    success: state.resetPassword.success,
    error: state.resetPassword.error,
    accountType: state.resetPassword.accountType,
    ongoingRequest: state.resetPassword.ongoingRequest,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetPassword: data => dispatch(resetPassword(data)),
    resetError: () => dispatch(resetError()),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPassword);
