import * as actions from '../actions/actionTypes';
const initialState = {
  ongoingRequest: false,
  success: false,
  error: null,
  accountType: '',
};
const resetPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_RESET_PASSWORD_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.RESET_RESET_PASSWORD_ERROR:
      return {
        ...state,
        error: null,
      };
    case actions.RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        success: true,
        accountType: action.data.accountType,
      };
    case actions.RESET_PASSWORD_ERROR:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};
export default resetPasswordReducer;
