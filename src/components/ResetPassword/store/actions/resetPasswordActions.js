import * as actions from './actionTypes';
import env from '../../../../config/environment';
const setOnGoingRequest = value => {
  return {
    type: actions.SET_RESET_PASSWORD_ONGOING_REQUEST,
    value,
  };
};
const resetPasswordSuccess = data => {
  return {
    type: actions.RESET_PASSWORD_SUCCESS,
    data,
  };
};
const resetPasswordError = error => {
  return {
    type: actions.RESET_PASSWORD_ERROR,
    error: error,
  };
};
export const resetError = () => {
  return {
    type: actions.RESET_RESET_PASSWORD_ERROR,
  };
};
export const resetPassword = data => {
  return dispatch => {
    dispatch(setOnGoingRequest(true));
    fetch(env.API_URL + 'auth/resetPassword', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOnGoingRequest(false));
        if (result.success === true) {
          dispatch(resetPasswordSuccess(result));
        } else {
          dispatch(resetPasswordError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOnGoingRequest(false));
        dispatch(resetPasswordError('Something went wrong'));
      });
  };
};
