import React, {PureComponent} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AspirantHomeTab from '../../components/AspirantHomeTab/AspirantHomeTab';
import AspirantProfileTab from '../../components/AspirantProfileTab/AspirantProfileTab';
import AspirantNotificationTab from '../../components/AspirantNotificationTab/AspirantNotificationTab';
import PathsEnrolledTab from '../../components/PathsEnrolledTab/PathsEnrolledTab';
import AspirantSearchTab from '../../components/AspirantSearchTab/AspirantSearchTab';
import Icon from 'react-native-vector-icons/FontAwesome5';
const Tab = createBottomTabNavigator();
class AspirantDashboard extends PureComponent {
  render() {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = 'home';
            } else if (route.name === 'Search') {
              iconName = 'search';
            } else if (route.name === 'PathEnrolled') {
              iconName = 'book-open';
            } else if (route.name === 'Notification') {
              iconName = 'bell';
            } else {
              iconName = 'user-circle';
            }
            return <Icon name={iconName} color={color} size={size} />;
          },
        })}
        tabBarOptions={{
          showLabel: false,
          activeTintColor: '#009fee',
          inactiveTintColor: 'rgb(227,227,227)',
        }}>
        <Tab.Screen name="Home" component={AspirantHomeTab} />
        <Tab.Screen name="Search" component={AspirantSearchTab} />
        <Tab.Screen name="PathEnrolled" component={PathsEnrolledTab} />
        <Tab.Screen name="Notification" component={AspirantNotificationTab} />
        <Tab.Screen name="Profile" component={AspirantProfileTab} />
      </Tab.Navigator>
    );
  }
}
export default AspirantDashboard;
