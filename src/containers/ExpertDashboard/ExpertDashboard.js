import React, {PureComponent} from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ExpertHomeTab from '../../components/ExpertHomeTab/ExpertHomeTab';
import ExpertProfileTab from '../../components/ExpertProfileTab/ExpertProfileTab';
import ExpertNotificationTab from '../../components/ExpertNotificationTab/ExpertNotificationTab';
import ExpertSearchTab from '../../components/ExpertSearchTab/ExpertSearchTab';
import PathsEnrolledTab from '../../components/PathsEnrolledTab/PathsEnrolledTab';
import Icon from 'react-native-vector-icons/FontAwesome5';
const Tab = createBottomTabNavigator();

class ExpertDashboard extends PureComponent {
  render() {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = 'home';
            } else if (route.name === 'Search') {
              iconName = 'search';
            } else if (route.name === 'PathEnrolled') {
              iconName = 'book-open';
            } else if (route.name === 'Notification') {
              iconName = 'bell';
            } else {
              iconName = 'user-circle';
            }
            return <Icon name={iconName} color={color} size={size} />;
          },
        })}
        tabBarOptions={{
          keyboardHidesTabBar: true,
          showLabel: false,
          activeTintColor: '#009fee',
          inactiveTintColor: 'rgb(227,227,227)',
        }}>
        <Tab.Screen name="Home" component={ExpertHomeTab} />
        <Tab.Screen name="Search" component={ExpertSearchTab} />
        <Tab.Screen name="PathEnrolled" component={PathsEnrolledTab} />
        <Tab.Screen name="Notification" component={ExpertNotificationTab} />
        <Tab.Screen name="Profile" component={ExpertProfileTab} />
      </Tab.Navigator>
    );
  }
}
export default ExpertDashboard;
