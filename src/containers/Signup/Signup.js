import React, {PureComponent} from 'react';
import {
  Platform,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  Modal,
  FlatList,
  Keyboard,
  ToastAndroid,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import KeyboardShift from '../../components/KeyboardShift/KeyboardShift';
import data from '../../assets/data/countries';
import {connect} from 'react-redux';
import {
  setEmail,
  setPhoneNumber,
  setCountryCode,
  setCountry,
  proceedSignup,
  resetError,
  resetSuccess,
} from './store/actions/index';
import Loader from '../../components/UI/Loader/Loader';
import validator from 'validator';
const defaultFlag = data.filter(obj => obj.name === 'India')[0].flag;
class Signup extends PureComponent {
  state = {
    flag: defaultFlag,
    modalVisible: false,
  };

  showModal = () => {
    this.setState({modalVisible: true});
  };
  closeModal = () => {
    this.setState({modalVisible: false});
  };
  getCountry = async country => {
    const countryData = await data;
    try {
      const countryCode = await countryData.filter(
        obj => obj.name === country,
      )[0].dial_code;
      const countryFlag = await countryData.filter(
        obj => obj.name === country,
      )[0].flag;
      const countryName = await countryData
        .filter(obj => obj.name === country)[0]
        .name.toLowerCase();
      this.setState({flag: countryFlag});
      this.props.setCountryCode(countryCode);
      this.props.setCountry(countryName);
      await this.closeModal();
    } catch (err) {
      console.log(err);
    }
  };
  signupButtonClicked = () => {
    console.log('sssss');
    const data = {
      phoneNumber: this.props.countryCode + this.props.phoneNumber,
      email: this.props.email.toLowerCase(),
    };
    this.props.proceedSignup(data);
  };
  render() {
    if (this.props.success) {
      this.props.resetSuccess();
      this.props.navigation.navigate('VerifyOtp');
    }
    if (this.props.error && Platform.OS === 'android') {
      ToastAndroid.show(this.props.error, ToastAndroid.SHORT);
      this.props.resetError();
    }
    if (this.props.error && Platform.OS === 'ios') {
      Alert.alert(this.props.error);
      this.props.resetError();
    }
    let isDisabled = false;
    const buttonStyles = [styles.signupButton];
    if (
      !validator.isNumeric(this.props.phoneNumber) ||
      this.props.phoneNumber.length != 10 ||
      !validator.isEmail(this.props.email)
    ) {
      isDisabled = true;
      buttonStyles.push(styles.disablecss);
    }
    const countryData = data;
    return (
      <KeyboardShift>
        {() => (
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <SafeAreaView style={styles.container}>
              {this.props.ongoingRequest && <Loader />}
              <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}>
                <SafeAreaView style={styles.container}>
                  <View style={styles.subContainer}>
                    <Icon
                      name="times"
                      size={25}
                      style={styles.modalCloseIcon}
                      onPress={this.closeModal}
                    />
                    <FlatList
                      style={{height: '90%', width: '100%'}}
                      data={countryData}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({item}) => (
                        <TouchableWithoutFeedback
                          onPress={() => this.getCountry(item.name)}>
                          <View
                            style={[
                              styles.countryStyle,
                              {
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'flex-start',
                              },
                            ]}>
                            <Text style={{fontSize: 45}}>{item.flag}</Text>
                            <Text style={{fontSize: 20, color: '#fff'}}>
                              {item.name} ({item.dial_code})
                            </Text>
                          </View>
                        </TouchableWithoutFeedback>
                      )}
                    />
                  </View>
                </SafeAreaView>
              </Modal>
              <View style={styles.subContainer}>
                <Icon
                  name="arrow-left"
                  size={20}
                  style={styles.backIcon}
                  onPress={() => this.props.navigation.goBack()}
                />
                <Text style={styles.signupHeading}>
                  {' '}
                  Let's get you signed up{' '}
                </Text>

                <View style={styles.phoneNumberView}>
                  <TouchableWithoutFeedback onPress={() => this.showModal()}>
                    <View style={styles.countryCodeInput}>
                      <Text style={styles.countryCodeText}>
                        {' '}
                        {this.state.flag + ' ' + this.props.countryCode}{' '}
                      </Text>
                      <Icon active name="caret-down" style={styles.iconArrow} />
                    </View>
                  </TouchableWithoutFeedback>
                  <TextInput
                    keyboardType="phone-pad"
                    placeholderTextColor="rgb(227,227,227)"
                    style={styles.numberInput}
                    placeholder="Phone Number"
                    value={this.props.phoneNumber}
                    onChangeText={text => this.props.setPhoneNumber(text)}
                  />
                </View>
                <TextInput
                  value={this.props.email}
                  onChangeText={text => this.props.setEmail(text)}
                  keyboardType="email-address"
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Email Address"
                />
                <Text style={styles.termsAgreeText}>
                  By signing up you are agreeing to our terms & conditions
                </Text>
                <TouchableOpacity
                  disabled={isDisabled}
                  style={buttonStyles}
                  onPress={() => this.signupButtonClicked()}>
                  <Text style={styles.signupButtonText}> Sign Up </Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </TouchableWithoutFeedback>
        )}
      </KeyboardShift>
    );
  }
}
const styles = StyleSheet.create({
  disablecss: {
    opacity: 0.4,
  },
  signupButton: {
    display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '100%',
  },
  signupButtonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  termsAgreeText: {
    fontFamily: 'Roboto',
    width: '100%',
    fontSize: 13,
    color: 'rgb(168,168,168)',
    fontWeight: '600',
    marginBottom: 30,
  },
  countryStyle: {
    flex: 1,
    backgroundColor: '#009fee',
    borderTopColor: '#fff',
    borderTopWidth: 1,
    padding: 12,
  },
  modalCloseIcon: {
    alignSelf: 'flex-end',
    height: '10%',
  },
  countryCodeText: {
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  iconArrow: {
    fontSize: 20,
    alignSelf: 'center',
  },

  container: {
    flex: 1,
  },
  subContainer: {
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',

    alignItems: 'flex-start',
    padding: '6%',
  },
  backIcon: {
    marginTop: '5%',
    marginBottom: '5%',
  },
  signupHeading: {
    fontFamily: 'Roboto',
    fontWeight: '900',
    fontSize: 28,
  },
  phoneNumberView: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  countryCodeInput: {
    display: 'flex',
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginBottom: 30,
    paddingBottom: 20,
  },
  numberInput: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '65%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
});
const mapStateToProps = state => {
  return {
    countryCode: state.signup.countryCode,
    phoneNumber: state.signup.phoneNumber,
    email: state.signup.email,
    error: state.signup.error,
    success: state.signup.success,
    ongoingRequest: state.signup.ongoingRequest,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setPhoneNumber: data => dispatch(setPhoneNumber(data)),
    setCountryCode: data => dispatch(setCountryCode(data)),
    setCountry: data => dispatch(setCountry(data)),
    setEmail: data => dispatch(setEmail(data)),
    proceedSignup: data => dispatch(proceedSignup(data)),
    resetError: () => dispatch(resetError()),
    resetSuccess: () => dispatch(resetSuccess()),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Signup);
