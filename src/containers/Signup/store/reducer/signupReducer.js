import * as actions from '../actions/actionTypes';
const initialState = {
  ongoingRequest: false,
  success: false,
  error: null,
  countryCode: '+91',
  phoneNumber: '',
  country: 'india',
  email: '',
};
const signupReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.PROCEED_SIGNUP_SET_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.PROCEED_SIGNUP_RESET_SUCCESS_STATUS:
      return {
        ...state,
        success: false,
      };
    case actions.PROCEED_SIGNUP_RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    case actions.PROCEED_SIGNUP_SUCCESS:
      return {
        ...state,
        success: action.value,
      };
    case actions.PROCEED_SIGNUP_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actions.SET_SIGNUP_EMAIL:
      return {
        ...state,
        email: action.value,
      };
    case actions.SET_COUNTRY:
      return {
        ...state,
        country: action.value,
      };
    case actions.SET_COUNTRY_CODE:
      return {
        ...state,
        countryCode: action.value,
      };
    case actions.SET_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.value,
      };
    default:
      return state;
  }
};
export default signupReducer;
