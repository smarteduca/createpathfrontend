import * as actions from './actionTypes';
import env from '../../../../config/environment';
export const setCountryCode = value => {
  return {
    type: actions.SET_COUNTRY_CODE,
    value: value,
  };
};
export const setPhoneNumber = value => {
  return {
    type: actions.SET_PHONE_NUMBER,
    value: value,
  };
};
export const setCountry = value => {
  return {
    type: actions.SET_COUNTRY,
    value: value,
  };
};
export const setEmail = value => {
  return {
    type: actions.SET_SIGNUP_EMAIL,
    value: value,
  };
};
const proceedSignupSuccess = value => {
  return {
    type: actions.PROCEED_SIGNUP_SUCCESS,
    value: value,
  };
};
const proceedSignupError = error => {
  return {
    type: actions.PROCEED_SIGNUP_ERROR,
    error: error,
  };
};
const sendCode = data => {
  return dispatch => {
    fetch(env.API_URL + 'verify/sendVerification', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOngoingRequest(false));
        if (result.verification.status === 'pending') {
          dispatch(proceedSignupSuccess(true));
        } else {
          dispatch(proceedSignupError('Something went wrong'));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(proceedSignupError('Invalid JSON'));
      });
  };
};
const setOngoingRequest = value => {
  return {
    type: actions.PROCEED_SIGNUP_SET_ONGOING_REQUEST,
    value,
  };
};
export const proceedSignup = data => {
  return dispatch => {
    dispatch(setOngoingRequest(true));
    fetch(env.API_URL + 'auth/proceedSignup', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        if (result.success === true) {
          dispatch(sendCode(data));
        } else {
          dispatch(setOngoingRequest(false));
          dispatch(proceedSignupError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOngoingRequest(false));
        dispatch(proceedSignupError('Invalid JSON'));
      });
  };
};
export const resetError = () => {
  return {
    type: actions.PROCEED_SIGNUP_RESET_ERROR,
  };
};
export const resetSuccess = () => {
  return {
    type: actions.PROCEED_SIGNUP_RESET_SUCCESS_STATUS,
  };
};
