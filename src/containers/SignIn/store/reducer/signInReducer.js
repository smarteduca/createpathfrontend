import * as actions from '../actions/actionTypes';
const initialState = {
  ongoingRequest: false,
  success: false,
  error: null,
  accountType: null,
  phoneNumber: '',
  countryCode: '+91',
};
const signInReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_SIGNIN_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.value,
      };
    case actions.SET_SIGNIN_COUNTRY_CODE:
      return {
        ...state,
        countryCode: action.value,
      };
    case actions.SIGNIN_SUCCESS:
      return {
        ...state,
        success: true,
        accountType: action.value,
      };
    case actions.SIGNIN_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actions.SIGNIN_ONGOING_REQUEST:
      return {
        ...state,
        ongoingRequest: action.value,
      };
    case actions.SIGNIN_RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
export default signInReducer;
