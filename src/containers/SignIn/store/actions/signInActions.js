import * as actions from './actionTypes';
import AsyncStorage from '@react-native-community/async-storage';
import env from '../../../../config/environment';
export const resetSignInError = () => {
  return {
    type: actions.SIGNIN_RESET_ERROR,
  };
};
const signInSuccess = value => {
  return {
    type: actions.SIGNIN_SUCCESS,
    value,
  };
};
const signInError = error => {
  return {
    type: actions.SIGNIN_ERROR,
    error,
  };
};
const setOnGoingRequest = value => {
  return {
    type: actions.SIGNIN_ONGOING_REQUEST,
    value,
  };
};
export const signIn = data => {
  return dispatch => {
    dispatch(setOnGoingRequest(true));
    fetch(env.API_URL + 'auth/login', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(res => {
        return res.json();
      })
      .then(result => {
        dispatch(setOnGoingRequest(false));
        if (result.success) {
          const token = result.token;
          const accountType = result.accountType;
          AsyncStorage.setItem('token', token)
            .then(result => {
              dispatch(signInSuccess(accountType));
            })
            .catch(err => {
              dispatch(signInError('Something went wrong'));
            });
        } else {
          dispatch(signInError(result.message));
        }
      })
      .catch(err => {
        dispatch(setOnGoingRequest(false));
        dispatch(signInError('Invalid JSON'));
      });
  };
};
export const setPhoneNumber = value => {
  return {
    type: actions.SET_SIGNIN_PHONE_NUMBER,
    value,
  };
};
export const setSignInCountryCode = value => {
  return {
    type: actions.SET_SIGNIN_COUNTRY_CODE,
    value,
  };
};
