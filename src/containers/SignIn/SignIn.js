import React, {PureComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Platform,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  TouchableWithoutFeedback,
  Alert,
  ToastAndroid,
  FlatList,
  Modal,
} from 'react-native';

import Loader from '../../components/UI/Loader/Loader';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import {
  resetSignInError,
  signIn,
  setPhoneNumber,
  setSignInCountryCode,
} from './store/actions/index';
import data from '../../assets/data/countries';
import validator from 'validator';
const defaultFlag = data.filter(obj => obj.name === 'India')[0].flag;
class SignIn extends PureComponent {
  state = {
    showPassword: false,
    flag: defaultFlag,
    password: {
      value: '',
      valid: false,
    },
    modalVisible: false,
  };
  openModal = () => {
    this.setState({modalVisible: true});
  };
  closeModal = () => {
    this.setState({modalVisible: false});
  };
  getCountry = async country => {
    const countryData = await data;
    try {
      const countryCode = await countryData.filter(
        obj => obj.name === country,
      )[0].dial_code;
      const countryFlag = await countryData.filter(
        obj => obj.name === country,
      )[0].flag;
      const countryName = await countryData
        .filter(obj => obj.name === country)[0]
        .name.toLowerCase();
      this.setState({flag: countryFlag});
      this.props.setSignInCountryCode(countryCode);

      this.closeModal();
    } catch (err) {
      console.log(err);
    }
  };
  toggleShowPassword = () => {
    const showPassword = this.state.showPassword;
    this.setState({showPassword: !showPassword});
  };

  updatePassword = (propertyName, updatedValue) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [propertyName]: {
          ...prevState.propertyName,
          value: updatedValue,
          valid: validator.isLength(updatedValue, {
            min: 8,
          }),
        },
      };
    });
  };
  signIn = () => {
    const data = {
      phoneNumber: this.props.countryCode + this.props.phoneNumber,
      password: this.state.password.value,
    };
    this.props.signIn(data);
  };

  render() {
    const countryData = data;
    const signInButtonStyles = [styles.signinButton];
    if (
      !validator.isNumeric(this.props.phoneNumber) ||
      this.props.phoneNumber.length != 10 ||
      !this.state.password.valid
    ) {
      signInButtonStyles.push(styles.disablecss);
    }
    if (this.props.success && this.props.accountType === 'aspirant') {
      this.props.navigation.navigate('AspirantDashboard');
    }
    if (this.props.success && this.props.accountType === 'expert') {
      this.props.navigation.navigate('ExpertDashboard');
    }
    if (this.props.error && Platform.OS === 'android') {
      ToastAndroid.show(this.props.error, ToastAndroid.SHORT);
      this.props.resetSignInError();
    }
    if (this.props.error && Platform.OS === 'ios') {
      Alert.alert(this.props.error);
      this.props.resetSignInError();
    }
    return (
      <SafeAreaView style={styles.container}>
        {this.props.ongoingRequest && <Loader />}
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}>
          <SafeAreaView style={styles.container}>
            <View style={styles.subContainer}>
              <Icon
                name="times"
                size={25}
                style={styles.modalCloseIcon}
                onPress={this.closeModal}
              />
              <FlatList
                style={{height: '90%', width: '100%'}}
                data={countryData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (
                  <TouchableWithoutFeedback
                    onPress={() => this.getCountry(item.name)}>
                    <View
                      style={[
                        styles.countryStyle,
                        {
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'flex-start',
                        },
                      ]}>
                      <Text style={{fontSize: 45}}>{item.flag}</Text>
                      <Text style={{fontSize: 20, color: '#fff'}}>
                        {item.name} ({item.dial_code})
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              />
            </View>
          </SafeAreaView>
        </Modal>
        <View style={styles.subContainer}>
          <Icon
            name="arrow-left"
            size={20}
            style={styles.backIcon}
            onPress={() => this.props.navigation.goBack()}
          />
          <Text style={styles.signinHeading}> Sign In into StepIt </Text>
          <View style={styles.phoneNumberView}>
            <TouchableWithoutFeedback onPress={() => this.openModal()}>
              <View style={styles.countryCodeInput}>
                <Text style={styles.countryCodeText}>
                  {' '}
                  {this.state.flag + ' ' + this.props.countryCode}{' '}
                </Text>
                <Icon active name="caret-down" style={styles.iconArrow} />
              </View>
            </TouchableWithoutFeedback>
            <TextInput
              keyboardType="phone-pad"
              placeholderTextColor="rgb(227,227,227)"
              style={styles.numberInput}
              placeholder="Phone Number"
              value={this.props.phoneNumber}
              onChangeText={text => this.props.setPhoneNumber(text)}
            />
          </View>
          <View style={styles.passwordInput}>
            <Icon
              onPress={this.toggleShowPassword}
              style={styles.passwordIcon}
              name={this.state.showPassword ? 'eye' : 'eye-slash'}
              size={20}
              color="rgb(227,227,227)"
            />
            <TextInput
              value={this.state.password}
              onChangeText={text => this.updatePassword('password', text)}
              secureTextEntry={!this.state.showPassword}
              textContentType="password"
              placeholderTextColor="rgb(227,227,227)"
              style={styles.input}
              placeholder="Password"
            />
          </View>
          <TouchableOpacity
            style={{display: 'flex', width: '100%'}}
            onPress={() => this.props.navigation.navigate('ForgotPassword')}>
            <Text style={styles.forgotPasswordText}> Forgot Password? </Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={
              !this.state.password.valid ||
              !validator.isNumeric(this.props.phoneNumber) ||
              this.props.phoneNumber.length != 10
            }
            style={signInButtonStyles}
            onPress={() => this.signIn()}>
            <Text style={styles.signinButtonText}> Sign In </Text>
          </TouchableOpacity>
          <View style={styles.signupTextWrapper}>
            <Text style={styles.signupInfo}> Don't Have an account ? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('AccountType')}>
              <Text style={styles.signupText}> SignUp </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  numberInput: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '65%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  iconArrow: {
    fontSize: 20,
    alignSelf: 'center',
  },
  countryCodeInput: {
    display: 'flex',
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgb(227,227,227)',
    borderBottomWidth: 2,
    marginBottom: 30,
    paddingBottom: 20,
  },
  countryCodeText: {
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  phoneNumberView: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  countryStyle: {
    flex: 1,
    backgroundColor: '#009fee',
    borderTopColor: '#fff',
    borderTopWidth: 1,
    padding: 12,
  },
  modalCloseIcon: {
    alignSelf: 'flex-end',
    height: '10%',
  },
  disablecss: {
    opacity: 0.4,
  },
  signupText: {
    color: '#009fee',
    fontSize: 15,
    fontFamily: 'Roboto',
  },
  signupInfo: {
    color: 'gray',
    fontSize: 15,
    fontWeight: '400',
  },
  signupTextWrapper: {
    display: 'flex',
    width: '100%',

    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center',
  },
  signinButtonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto',
  },
  signinButton: {
    display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '100%',
  },
  forgotPasswordText: {
    marginBottom: 30,
    fontSize: 15,
    alignSelf: 'flex-end',
    color: '#009fee',
    fontWeight: 'bold',
  },
  passwordInput: {
    display: 'flex',
    width: '100%',
    position: 'relative',
  },
  passwordIcon: {
    position: 'absolute',
    right: 0,
    bottom: 50,
    zIndex: 1,
  },
  container: {
    flex: 1,
  },
  subContainer: {
    padding: '6%',
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',

    alignItems: 'flex-start',
  },
  signinHeading: {
    fontFamily: 'Roboto',
    fontWeight: '900',
    fontSize: 28,
  },
  backIcon: {
    marginTop: '5%',
    marginBottom: '5%',
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
});
const mapStateToProps = state => {
  return {
    phoneNumber: state.signin.phoneNumber,
    countryCode: state.signin.countryCode,
    ongoingRequest: state.signin.ongoingRequest,
    success: state.signin.success,
    error: state.signin.error,
    accountType: state.signin.accountType,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    resetSignInError: () => dispatch(resetSignInError()),
    signIn: data => dispatch(signIn(data)),

    setPhoneNumber: data => dispatch(setPhoneNumber(data)),
    setSignInCountryCode: data => dispatch(setSignInCountryCode(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignIn);
